﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace sistemas.Web.Data.Entities
{
    public class HistorialEquiUsEntity
    {
        public int Id { get; set; }

        public EquipoEntity Equipos { get; set; }
        public UsuarioEntity Usuarios { get; set; }        

        public DateTime Fecha_Inicio { get; set; }

        [Display(Name = "Fecha Inicio")]
        [Required(ErrorMessage = "El campo {0} es obligatorio.")]
        [DisplayFormat(DataFormatString = "{0:yyyy/MM/dd}", ApplyFormatInEditMode = true)]
        public DateTime Fecha_InicioLocal => Fecha_Inicio.ToLocalTime();

        public DateTime Fecha_Final { get; set; }

        [Display(Name = "Fecha Final")]
        [Required(ErrorMessage = "El campo {0} es obligatorio.")]
        [DisplayFormat(DataFormatString = "{0:yyyy/MM/dd}", ApplyFormatInEditMode = true)]

        public DateTime Fecha_FinalLocal => Fecha_Final.ToLocalTime();

        


    }
}
