﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace sistemas.Web.Data.Entities
{
    public class EquipoSoftwareEntity
    {
        public int Id { get; set; }
        [Required(ErrorMessage = "El campo {0} es obligatorio.")]
        public EquipoEntity Equipos { get; set; }

        [Required(ErrorMessage = "El campo {0} es obligatorio.")]
        public SoftwareEntity Softwares { get; set; }
    }
}
