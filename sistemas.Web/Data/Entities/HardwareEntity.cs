﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace sistemas.Web.Data.Entities
{
    public class HardwareEntity
    {
        public int Id { get; set; }

        [StringLength(50, MinimumLength = 1, ErrorMessage = "El campo {0} debe tener al menos {1} caracteres.")]
        [Required(ErrorMessage = "El campo {0} es obligatorio.")]
        public string Description { get; set; }
        public string Serial { get; set; }

        public TipoEntity Tipos { get; set; }
    }
}
