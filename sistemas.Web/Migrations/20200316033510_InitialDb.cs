﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace sistemas.Web.Migrations
{
    public partial class InitialDb : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Sedes",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Description = table.Column<string>(maxLength: 40, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Sedes", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Tipos",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Tipo_Id = table.Column<string>(maxLength: 20, nullable: false),
                    Description = table.Column<string>(maxLength: 30, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Tipos", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Usuarios",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Cedula = table.Column<int>(nullable: false),
                    Nombres = table.Column<string>(maxLength: 40, nullable: false),
                    Apellidos = table.Column<string>(maxLength: 40, nullable: false),
                    Cargo = table.Column<string>(maxLength: 50, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Usuarios", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Equipos",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Codigo = table.Column<int>(nullable: false),
                    Marca = table.Column<string>(maxLength: 40, nullable: false),
                    Description = table.Column<string>(maxLength: 40, nullable: false),
                    DescriptionDetallada = table.Column<string>(nullable: true),
                    Serial = table.Column<string>(nullable: true),
                    Fecha_Compra = table.Column<DateTime>(nullable: false),
                    Valor = table.Column<decimal>(nullable: false),
                    TiposId = table.Column<int>(nullable: false),
                    SedesId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Equipos", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Equipos_Sedes_SedesId",
                        column: x => x.SedesId,
                        principalTable: "Sedes",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_Equipos_Tipos_TiposId",
                        column: x => x.TiposId,
                        principalTable: "Tipos",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "Hardwares",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Description = table.Column<string>(maxLength: 50, nullable: false),
                    Serial = table.Column<string>(nullable: true),
                    TiposId = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Hardwares", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Hardwares_Tipos_TiposId",
                        column: x => x.TiposId,
                        principalTable: "Tipos",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "Softwares",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Description = table.Column<string>(maxLength: 40, nullable: false),
                    DescriptionDetallada = table.Column<string>(nullable: true),
                    Serial = table.Column<string>(nullable: true),
                    TiposId = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Softwares", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Softwares_Tipos_TiposId",
                        column: x => x.TiposId,
                        principalTable: "Tipos",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "HistorialEquiUsEntity",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    EquiposId = table.Column<int>(nullable: true),
                    UsuariosId = table.Column<int>(nullable: true),
                    Fecha_Inicio = table.Column<DateTime>(nullable: false),
                    Fecha_Final = table.Column<DateTime>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_HistorialEquiUsEntity", x => x.Id);
                    table.ForeignKey(
                        name: "FK_HistorialEquiUsEntity_Equipos_EquiposId",
                        column: x => x.EquiposId,
                        principalTable: "Equipos",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_HistorialEquiUsEntity_Usuarios_UsuariosId",
                        column: x => x.UsuariosId,
                        principalTable: "Usuarios",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "EquipoHardwares",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    EquiposId = table.Column<int>(nullable: false),
                    HardwaresId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_EquipoHardwares", x => x.Id);
                    table.ForeignKey(
                        name: "FK_EquipoHardwares_Equipos_EquiposId",
                        column: x => x.EquiposId,
                        principalTable: "Equipos",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_EquipoHardwares_Hardwares_HardwaresId",
                        column: x => x.HardwaresId,
                        principalTable: "Hardwares",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "EquipoSoftwares",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    EquiposId = table.Column<int>(nullable: false),
                    SoftwaresId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_EquipoSoftwares", x => x.Id);
                    table.ForeignKey(
                        name: "FK_EquipoSoftwares_Equipos_EquiposId",
                        column: x => x.EquiposId,
                        principalTable: "Equipos",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_EquipoSoftwares_Softwares_SoftwaresId",
                        column: x => x.SoftwaresId,
                        principalTable: "Softwares",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "Mantenimientos",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Description = table.Column<string>(maxLength: 30, nullable: false),
                    HistorialEquiUsId = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Mantenimientos", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Mantenimientos_HistorialEquiUsEntity_HistorialEquiUsId",
                        column: x => x.HistorialEquiUsId,
                        principalTable: "HistorialEquiUsEntity",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateIndex(
                name: "IX_EquipoHardwares_EquiposId",
                table: "EquipoHardwares",
                column: "EquiposId");

            migrationBuilder.CreateIndex(
                name: "IX_EquipoHardwares_HardwaresId",
                table: "EquipoHardwares",
                column: "HardwaresId");

            migrationBuilder.CreateIndex(
                name: "IX_Equipos_SedesId",
                table: "Equipos",
                column: "SedesId");

            migrationBuilder.CreateIndex(
                name: "IX_Equipos_TiposId",
                table: "Equipos",
                column: "TiposId");

            migrationBuilder.CreateIndex(
                name: "IX_EquipoSoftwares_EquiposId",
                table: "EquipoSoftwares",
                column: "EquiposId");

            migrationBuilder.CreateIndex(
                name: "IX_EquipoSoftwares_SoftwaresId",
                table: "EquipoSoftwares",
                column: "SoftwaresId");

            migrationBuilder.CreateIndex(
                name: "IX_Hardwares_TiposId",
                table: "Hardwares",
                column: "TiposId");

            migrationBuilder.CreateIndex(
                name: "IX_HistorialEquiUsEntity_EquiposId",
                table: "HistorialEquiUsEntity",
                column: "EquiposId");

            migrationBuilder.CreateIndex(
                name: "IX_HistorialEquiUsEntity_UsuariosId",
                table: "HistorialEquiUsEntity",
                column: "UsuariosId");

            migrationBuilder.CreateIndex(
                name: "IX_Mantenimientos_HistorialEquiUsId",
                table: "Mantenimientos",
                column: "HistorialEquiUsId");

            migrationBuilder.CreateIndex(
                name: "IX_Softwares_TiposId",
                table: "Softwares",
                column: "TiposId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "EquipoHardwares");

            migrationBuilder.DropTable(
                name: "EquipoSoftwares");

            migrationBuilder.DropTable(
                name: "Mantenimientos");

            migrationBuilder.DropTable(
                name: "Hardwares");

            migrationBuilder.DropTable(
                name: "Softwares");

            migrationBuilder.DropTable(
                name: "HistorialEquiUsEntity");

            migrationBuilder.DropTable(
                name: "Equipos");

            migrationBuilder.DropTable(
                name: "Usuarios");

            migrationBuilder.DropTable(
                name: "Sedes");

            migrationBuilder.DropTable(
                name: "Tipos");
        }
    }
}
