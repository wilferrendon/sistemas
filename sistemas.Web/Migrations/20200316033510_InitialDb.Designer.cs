﻿// <auto-generated />
using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;
using Microsoft.EntityFrameworkCore.Storage.ValueConversion;
using sistemas.Web.Data;

namespace sistemas.Web.Migrations
{
    [DbContext(typeof(DataContext))]
    [Migration("20200316033510_InitialDb")]
    partial class InitialDb
    {
        protected override void BuildTargetModel(ModelBuilder modelBuilder)
        {
#pragma warning disable 612, 618
            modelBuilder
                .HasAnnotation("ProductVersion", "3.1.2")
                .HasAnnotation("Relational:MaxIdentifierLength", 128)
                .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

            modelBuilder.Entity("sistemas.Web.Data.Entities.EquipoEntity", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("int")
                        .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

                    b.Property<int>("Codigo")
                        .HasColumnType("int");

                    b.Property<string>("Description")
                        .IsRequired()
                        .HasColumnType("nvarchar(40)")
                        .HasMaxLength(40);

                    b.Property<string>("DescriptionDetallada")
                        .HasColumnType("nvarchar(max)");

                    b.Property<DateTime>("Fecha_Compra")
                        .HasColumnType("datetime2");

                    b.Property<string>("Marca")
                        .IsRequired()
                        .HasColumnType("nvarchar(40)")
                        .HasMaxLength(40);

                    b.Property<int>("SedesId")
                        .HasColumnType("int");

                    b.Property<string>("Serial")
                        .HasColumnType("nvarchar(max)");

                    b.Property<int>("TiposId")
                        .HasColumnType("int");

                    b.Property<decimal>("Valor")
                        .HasColumnType("decimal(18,2)");

                    b.HasKey("Id");

                    b.HasIndex("SedesId");

                    b.HasIndex("TiposId");

                    b.ToTable("Equipos");
                });

            modelBuilder.Entity("sistemas.Web.Data.Entities.EquipoHarwareEntity", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("int")
                        .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

                    b.Property<int>("EquiposId")
                        .HasColumnType("int");

                    b.Property<int>("HardwaresId")
                        .HasColumnType("int");

                    b.HasKey("Id");

                    b.HasIndex("EquiposId");

                    b.HasIndex("HardwaresId");

                    b.ToTable("EquipoHardwares");
                });

            modelBuilder.Entity("sistemas.Web.Data.Entities.EquipoSoftwareEntity", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("int")
                        .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

                    b.Property<int>("EquiposId")
                        .HasColumnType("int");

                    b.Property<int>("SoftwaresId")
                        .HasColumnType("int");

                    b.HasKey("Id");

                    b.HasIndex("EquiposId");

                    b.HasIndex("SoftwaresId");

                    b.ToTable("EquipoSoftwares");
                });

            modelBuilder.Entity("sistemas.Web.Data.Entities.HardwareEntity", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("int")
                        .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

                    b.Property<string>("Description")
                        .IsRequired()
                        .HasColumnType("nvarchar(50)")
                        .HasMaxLength(50);

                    b.Property<string>("Serial")
                        .HasColumnType("nvarchar(max)");

                    b.Property<int?>("TiposId")
                        .HasColumnType("int");

                    b.HasKey("Id");

                    b.HasIndex("TiposId");

                    b.ToTable("Hardwares");
                });

            modelBuilder.Entity("sistemas.Web.Data.Entities.HistorialEquiUsEntity", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("int")
                        .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

                    b.Property<int?>("EquiposId")
                        .HasColumnType("int");

                    b.Property<DateTime>("Fecha_Final")
                        .HasColumnType("datetime2");

                    b.Property<DateTime>("Fecha_Inicio")
                        .HasColumnType("datetime2");

                    b.Property<int?>("UsuariosId")
                        .HasColumnType("int");

                    b.HasKey("Id");

                    b.HasIndex("EquiposId");

                    b.HasIndex("UsuariosId");

                    b.ToTable("HistorialEquiUsEntity");
                });

            modelBuilder.Entity("sistemas.Web.Data.Entities.MantenimientoEntity", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("int")
                        .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

                    b.Property<string>("Description")
                        .IsRequired()
                        .HasColumnType("nvarchar(30)")
                        .HasMaxLength(30);

                    b.Property<int?>("HistorialEquiUsId")
                        .HasColumnType("int");

                    b.HasKey("Id");

                    b.HasIndex("HistorialEquiUsId");

                    b.ToTable("Mantenimientos");
                });

            modelBuilder.Entity("sistemas.Web.Data.Entities.SedeEntity", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("int")
                        .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

                    b.Property<string>("Description")
                        .IsRequired()
                        .HasColumnType("nvarchar(40)")
                        .HasMaxLength(40);

                    b.HasKey("Id");

                    b.ToTable("Sedes");
                });

            modelBuilder.Entity("sistemas.Web.Data.Entities.SoftwareEntity", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("int")
                        .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

                    b.Property<string>("Description")
                        .IsRequired()
                        .HasColumnType("nvarchar(40)")
                        .HasMaxLength(40);

                    b.Property<string>("DescriptionDetallada")
                        .HasColumnType("nvarchar(max)");

                    b.Property<string>("Serial")
                        .HasColumnType("nvarchar(max)");

                    b.Property<int?>("TiposId")
                        .HasColumnType("int");

                    b.HasKey("Id");

                    b.HasIndex("TiposId");

                    b.ToTable("Softwares");
                });

            modelBuilder.Entity("sistemas.Web.Data.Entities.TipoEntity", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("int")
                        .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

                    b.Property<string>("Description")
                        .IsRequired()
                        .HasColumnType("nvarchar(30)")
                        .HasMaxLength(30);

                    b.Property<string>("Tipo_Id")
                        .IsRequired()
                        .HasColumnType("nvarchar(20)")
                        .HasMaxLength(20);

                    b.HasKey("Id");

                    b.ToTable("Tipos");
                });

            modelBuilder.Entity("sistemas.Web.Data.Entities.UsuarioEntity", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("int")
                        .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

                    b.Property<string>("Apellidos")
                        .IsRequired()
                        .HasColumnType("nvarchar(40)")
                        .HasMaxLength(40);

                    b.Property<string>("Cargo")
                        .IsRequired()
                        .HasColumnType("nvarchar(50)")
                        .HasMaxLength(50);

                    b.Property<int>("Cedula")
                        .HasColumnType("int");

                    b.Property<string>("Nombres")
                        .IsRequired()
                        .HasColumnType("nvarchar(40)")
                        .HasMaxLength(40);

                    b.HasKey("Id");

                    b.ToTable("Usuarios");
                });

            modelBuilder.Entity("sistemas.Web.Data.Entities.EquipoEntity", b =>
                {
                    b.HasOne("sistemas.Web.Data.Entities.SedeEntity", "Sedes")
                        .WithMany()
                        .HasForeignKey("SedesId")
                        .OnDelete(DeleteBehavior.Cascade)
                        .IsRequired();

                    b.HasOne("sistemas.Web.Data.Entities.TipoEntity", "Tipos")
                        .WithMany()
                        .HasForeignKey("TiposId")
                        .OnDelete(DeleteBehavior.Cascade)
                        .IsRequired();
                });

            modelBuilder.Entity("sistemas.Web.Data.Entities.EquipoHarwareEntity", b =>
                {
                    b.HasOne("sistemas.Web.Data.Entities.EquipoEntity", "Equipos")
                        .WithMany()
                        .HasForeignKey("EquiposId")
                        .OnDelete(DeleteBehavior.Cascade)
                        .IsRequired();

                    b.HasOne("sistemas.Web.Data.Entities.HardwareEntity", "Hardwares")
                        .WithMany()
                        .HasForeignKey("HardwaresId")
                        .OnDelete(DeleteBehavior.Cascade)
                        .IsRequired();
                });

            modelBuilder.Entity("sistemas.Web.Data.Entities.EquipoSoftwareEntity", b =>
                {
                    b.HasOne("sistemas.Web.Data.Entities.EquipoEntity", "Equipos")
                        .WithMany()
                        .HasForeignKey("EquiposId")
                        .OnDelete(DeleteBehavior.Cascade)
                        .IsRequired();

                    b.HasOne("sistemas.Web.Data.Entities.SoftwareEntity", "Softwares")
                        .WithMany()
                        .HasForeignKey("SoftwaresId")
                        .OnDelete(DeleteBehavior.Cascade)
                        .IsRequired();
                });

            modelBuilder.Entity("sistemas.Web.Data.Entities.HardwareEntity", b =>
                {
                    b.HasOne("sistemas.Web.Data.Entities.TipoEntity", "Tipos")
                        .WithMany()
                        .HasForeignKey("TiposId");
                });

            modelBuilder.Entity("sistemas.Web.Data.Entities.HistorialEquiUsEntity", b =>
                {
                    b.HasOne("sistemas.Web.Data.Entities.EquipoEntity", "Equipos")
                        .WithMany()
                        .HasForeignKey("EquiposId");

                    b.HasOne("sistemas.Web.Data.Entities.UsuarioEntity", "Usuarios")
                        .WithMany()
                        .HasForeignKey("UsuariosId");
                });

            modelBuilder.Entity("sistemas.Web.Data.Entities.MantenimientoEntity", b =>
                {
                    b.HasOne("sistemas.Web.Data.Entities.HistorialEquiUsEntity", "HistorialEquiUs")
                        .WithMany()
                        .HasForeignKey("HistorialEquiUsId");
                });

            modelBuilder.Entity("sistemas.Web.Data.Entities.SoftwareEntity", b =>
                {
                    b.HasOne("sistemas.Web.Data.Entities.TipoEntity", "Tipos")
                        .WithMany()
                        .HasForeignKey("TiposId");
                });
#pragma warning restore 612, 618
        }
    }
}
