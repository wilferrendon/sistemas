﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using sistemas.Web.Data;
using sistemas.Web.Data.Entities;

namespace sistemas.Web.Controllers
{
    public class HistorialEquiUsesController : Controller
    {
        private readonly DataContext _context;

        public HistorialEquiUsesController(DataContext context)
        {
            _context = context;
        }

        // GET: HistorialEquiUses
        public async Task<IActionResult> Index()
        {
            return View(await _context.HistorialEquiUsEntity.ToListAsync());
        }

        // GET: HistorialEquiUses/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var historialEquiUsEntity = await _context.HistorialEquiUsEntity
                .FirstOrDefaultAsync(m => m.Id == id);
            if (historialEquiUsEntity == null)
            {
                return NotFound();
            }

            return View(historialEquiUsEntity);
        }

        // GET: HistorialEquiUses/Create
        public IActionResult Create()
        {
            return View();
        }

        // POST: HistorialEquiUses/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("Id,Description,Fecha_Inicio,Fecha_Final,Cargo")] HistorialEquiUsEntity historialEquiUsEntity)
        {
            if (ModelState.IsValid)
            {
                _context.Add(historialEquiUsEntity);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            return View(historialEquiUsEntity);
        }

        // GET: HistorialEquiUses/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var historialEquiUsEntity = await _context.HistorialEquiUsEntity.FindAsync(id);
            if (historialEquiUsEntity == null)
            {
                return NotFound();
            }
            return View(historialEquiUsEntity);
        }

        // POST: HistorialEquiUses/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("Id,Description,Fecha_Inicio,Fecha_Final,Cargo")] HistorialEquiUsEntity historialEquiUsEntity)
        {
            if (id != historialEquiUsEntity.Id)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(historialEquiUsEntity);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!HistorialEquiUsEntityExists(historialEquiUsEntity.Id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            return View(historialEquiUsEntity);
        }

        // GET: HistorialEquiUses/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var historialEquiUsEntity = await _context.HistorialEquiUsEntity
                .FirstOrDefaultAsync(m => m.Id == id);
            if (historialEquiUsEntity == null)
            {
                return NotFound();
            }

            return View(historialEquiUsEntity);
        }

        // POST: HistorialEquiUses/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var historialEquiUsEntity = await _context.HistorialEquiUsEntity.FindAsync(id);
            _context.HistorialEquiUsEntity.Remove(historialEquiUsEntity);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool HistorialEquiUsEntityExists(int id)
        {
            return _context.HistorialEquiUsEntity.Any(e => e.Id == id);
        }
    }
}
