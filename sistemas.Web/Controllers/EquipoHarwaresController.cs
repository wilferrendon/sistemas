﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using sistemas.Web.Data;
using sistemas.Web.Data.Entities;

namespace sistemas.Web.Controllers
{
    public class EquipoHarwaresController : Controller
    {
        private readonly DataContext _context;

        public EquipoHarwaresController(DataContext context)
        {
            _context = context;
        }

        // GET: EquipoHarwares
        public async Task<IActionResult> Index()
        {
            return View(await _context.EquipoHardwares.ToListAsync());
        }

        // GET: EquipoHarwares/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var equipoHarwareEntity = await _context.EquipoHardwares
                .FirstOrDefaultAsync(m => m.Id == id);
            if (equipoHarwareEntity == null)
            {
                return NotFound();
            }

            return View(equipoHarwareEntity);
        }

        // GET: EquipoHarwares/Create
        public IActionResult Create()
        {
            return View();
        }

        // POST: EquipoHarwares/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("Id")] EquipoHarwareEntity equipoHarwareEntity)
        {
            if (ModelState.IsValid)
            {
                _context.Add(equipoHarwareEntity);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            return View(equipoHarwareEntity);
        }

        // GET: EquipoHarwares/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var equipoHarwareEntity = await _context.EquipoHardwares.FindAsync(id);
            if (equipoHarwareEntity == null)
            {
                return NotFound();
            }
            return View(equipoHarwareEntity);
        }

        // POST: EquipoHarwares/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("Id")] EquipoHarwareEntity equipoHarwareEntity)
        {
            if (id != equipoHarwareEntity.Id)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(equipoHarwareEntity);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!EquipoHarwareEntityExists(equipoHarwareEntity.Id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            return View(equipoHarwareEntity);
        }

        // GET: EquipoHarwares/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var equipoHarwareEntity = await _context.EquipoHardwares
                .FirstOrDefaultAsync(m => m.Id == id);
            if (equipoHarwareEntity == null)
            {
                return NotFound();
            }

            return View(equipoHarwareEntity);
        }

        // POST: EquipoHarwares/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var equipoHarwareEntity = await _context.EquipoHardwares.FindAsync(id);
            _context.EquipoHardwares.Remove(equipoHarwareEntity);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool EquipoHarwareEntityExists(int id)
        {
            return _context.EquipoHardwares.Any(e => e.Id == id);
        }
    }
}
