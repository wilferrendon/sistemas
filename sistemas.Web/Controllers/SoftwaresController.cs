﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using sistemas.Web.Data;
using sistemas.Web.Data.Entities;

namespace sistemas.Web.Controllers
{
    public class SoftwaresController : Controller
    {
        private readonly DataContext _context;

        public SoftwaresController(DataContext context)
        {
            _context = context;
        }

        // GET: Softwares
        public async Task<IActionResult> Index()
        {
            return View(await _context.Softwares.ToListAsync());
        }

        // GET: Softwares/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var softwareEntity = await _context.Softwares
                .FirstOrDefaultAsync(m => m.Id == id);
            if (softwareEntity == null)
            {
                return NotFound();
            }

            return View(softwareEntity);
        }

        // GET: Softwares/Create
        public IActionResult Create()
        {
            return View();
        }

        // POST: Softwares/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("Id,Description,DescriptionDetallada,Serial")] SoftwareEntity softwareEntity)
        {
            if (ModelState.IsValid)
            {
                _context.Add(softwareEntity);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            return View(softwareEntity);
        }

        // GET: Softwares/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var softwareEntity = await _context.Softwares.FindAsync(id);
            if (softwareEntity == null)
            {
                return NotFound();
            }
            return View(softwareEntity);
        }

        // POST: Softwares/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("Id,Description,DescriptionDetallada,Serial")] SoftwareEntity softwareEntity)
        {
            if (id != softwareEntity.Id)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(softwareEntity);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!SoftwareEntityExists(softwareEntity.Id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            return View(softwareEntity);
        }

        // GET: Softwares/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var softwareEntity = await _context.Softwares
                .FirstOrDefaultAsync(m => m.Id == id);
            if (softwareEntity == null)
            {
                return NotFound();
            }

            return View(softwareEntity);
        }

        // POST: Softwares/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var softwareEntity = await _context.Softwares.FindAsync(id);
            _context.Softwares.Remove(softwareEntity);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool SoftwareEntityExists(int id)
        {
            return _context.Softwares.Any(e => e.Id == id);
        }
    }
}
