﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using sistemas.Web.Data;
using sistemas.Web.Data.Entities;

namespace sistemas.Web.Controllers
{
    public class EquipoSoftwaresController : Controller
    {
        private readonly DataContext _context;

        public EquipoSoftwaresController(DataContext context)
        {
            _context = context;
        }

        // GET: EquipoSoftwares
        public async Task<IActionResult> Index()
        {
            return View(await _context.EquipoSoftwares.ToListAsync());
        }

        // GET: EquipoSoftwares/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var equipoSoftwareEntity = await _context.EquipoSoftwares
                .FirstOrDefaultAsync(m => m.Id == id);
            if (equipoSoftwareEntity == null)
            {
                return NotFound();
            }

            return View(equipoSoftwareEntity);
        }

        // GET: EquipoSoftwares/Create
        public IActionResult Create()
        {
            return View();
        }

        // POST: EquipoSoftwares/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("Id")] EquipoSoftwareEntity equipoSoftwareEntity)
        {
            if (ModelState.IsValid)
            {
                _context.Add(equipoSoftwareEntity);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            return View(equipoSoftwareEntity);
        }

        // GET: EquipoSoftwares/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var equipoSoftwareEntity = await _context.EquipoSoftwares.FindAsync(id);
            if (equipoSoftwareEntity == null)
            {
                return NotFound();
            }
            return View(equipoSoftwareEntity);
        }

        // POST: EquipoSoftwares/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("Id")] EquipoSoftwareEntity equipoSoftwareEntity)
        {
            if (id != equipoSoftwareEntity.Id)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(equipoSoftwareEntity);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!EquipoSoftwareEntityExists(equipoSoftwareEntity.Id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            return View(equipoSoftwareEntity);
        }

        // GET: EquipoSoftwares/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var equipoSoftwareEntity = await _context.EquipoSoftwares
                .FirstOrDefaultAsync(m => m.Id == id);
            if (equipoSoftwareEntity == null)
            {
                return NotFound();
            }

            return View(equipoSoftwareEntity);
        }

        // POST: EquipoSoftwares/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var equipoSoftwareEntity = await _context.EquipoSoftwares.FindAsync(id);
            _context.EquipoSoftwares.Remove(equipoSoftwareEntity);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool EquipoSoftwareEntityExists(int id)
        {
            return _context.EquipoSoftwares.Any(e => e.Id == id);
        }
    }
}
