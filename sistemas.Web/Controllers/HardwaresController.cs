﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using sistemas.Web.Data;
using sistemas.Web.Data.Entities;

namespace sistemas.Web.Controllers
{
    public class HardwaresController : Controller
    {
        private readonly DataContext _context;

        public HardwaresController(DataContext context)
        {
            _context = context;
        }

        // GET: Hardwares
        public async Task<IActionResult> Index()
        {
            return View(await _context.Hardwares.ToListAsync());
        }

        // GET: Hardwares/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var hardwareEntity = await _context.Hardwares
                .FirstOrDefaultAsync(m => m.Id == id);
            if (hardwareEntity == null)
            {
                return NotFound();
            }

            return View(hardwareEntity);
        }

        // GET: Hardwares/Create
        public IActionResult Create()
        {
            return View();
        }

        // POST: Hardwares/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("Id,Description,Serial")] HardwareEntity hardwareEntity)
        {
            if (ModelState.IsValid)
            {
                _context.Add(hardwareEntity);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            return View(hardwareEntity);
        }

        // GET: Hardwares/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var hardwareEntity = await _context.Hardwares.FindAsync(id);
            if (hardwareEntity == null)
            {
                return NotFound();
            }
            return View(hardwareEntity);
        }

        // POST: Hardwares/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("Id,Description,Serial")] HardwareEntity hardwareEntity)
        {
            if (id != hardwareEntity.Id)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(hardwareEntity);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!HardwareEntityExists(hardwareEntity.Id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            return View(hardwareEntity);
        }

        // GET: Hardwares/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var hardwareEntity = await _context.Hardwares
                .FirstOrDefaultAsync(m => m.Id == id);
            if (hardwareEntity == null)
            {
                return NotFound();
            }

            return View(hardwareEntity);
        }

        // POST: Hardwares/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var hardwareEntity = await _context.Hardwares.FindAsync(id);
            _context.Hardwares.Remove(hardwareEntity);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool HardwareEntityExists(int id)
        {
            return _context.Hardwares.Any(e => e.Id == id);
        }
    }
}
