﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using sistemas.Web.Data;
using sistemas.Web.Data.Entities;
using sistemas.Web.Helpers;
using sistemas.Web.Models;

namespace sistemas.Web.Controllers
{
    public class EquiposController : Controller
    {
        private readonly DataContext _dataContext;
        private readonly ICombosHelper _combosHelper;

        public EquiposController(DataContext context,
            ICombosHelper combosHelper)
        {
            _dataContext = context;
            _combosHelper = combosHelper;
        }

        // GET: Equipos
        public async Task<IActionResult> Index()
        {
            // return View(await _dataContext.Equipos.Include(o => o.Tipos).ToListAsync());  
            await Task.Delay(TimeSpan.FromSeconds(5));

            var query = from u in _dataContext.Equipos
                               join t in _dataContext.Tipos on u.Tipos.Id equals t.Id
                               join ur in _dataContext.HistorialEquiUsEntity on u.Id equals ur.Equipos.Id
                               where ur.Fecha_Final == DateTime.MinValue
                               select new EquiposViewModel
                               {
                                   Id = u.Id,
                                   Codigo = u.Codigo,
                                   Description = u.Description,
                                   DescriptionDetallada = u.DescriptionDetallada,
                                   Fecha_Compra = u.Fecha_Compra,
                                   Marca = u.Marca,
                                   Serial = u.Serial,
                                   Valor = u.Valor,
                                   AspNetTypeId = u.Tipos.Description.ToString(),
                                   AspNetUsuarioId = ur.Usuarios.Nombres + " " + ur.Usuarios.Apellidos,
                               };

            return View(query);
        }

        // GET: Equipos/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var equipoEntity = await _dataContext.Equipos
                .FirstOrDefaultAsync(m => m.Id == id);
            if (equipoEntity == null)
            {
                return NotFound();
            }

            return View(equipoEntity);
        }

        // GET: Equipos/Create
        public IActionResult Create()
        {

            var max = _dataContext.Equipos.Count();

            var model = new EquiposViewModel
            {
                Codigo = Convert.ToInt32(max),
                AspNetTypes = _combosHelper.GetComboTipoEquipos("Equipos"),
                AspNetUsuarios = _combosHelper.GetComboUsuarios(),
                AspNetSedes = _combosHelper.GetComboSedes(),
            };
            return View(model);

        }

        // POST: Equipos/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create(EquiposViewModel model)
        {
            if (ModelState.IsValid)
            {

                var tipo = _dataContext.Tipos.FirstOrDefault(
                    o => o.Id.ToString()== model.AspNetTypeId);

                var sede = _dataContext.Sedes.FirstOrDefault(
                    o => o.Id.ToString() == model.AspNetSedeId);

                var Equipo = new EquipoEntity
                {
                    Codigo = model.Codigo,
                    Description = model.Description,
                    DescriptionDetallada = model.DescriptionDetallada,
                    Fecha_Compra = model.Fecha_Compra,
                    Marca = model.Marca,
                    Serial = model.Serial,
                    Tipos = tipo,
                    Valor = model.Valor,
                    Sedes = sede,
                    
                };

                _dataContext.Add(Equipo);

                var equs = _dataContext.HistorialEquiUsEntity.FirstOrDefault(o => o.Equipos.Codigo == model.Codigo
                && o.Usuarios.Id.ToString() == model.AspNetUsuarioId);

                if (equs == null)
                {
                    var user = _dataContext.Usuarios.FirstOrDefault(o => o.Id.ToString()== model.AspNetUsuarioId);

                    var EquiUs = new HistorialEquiUsEntity
                    {
                        Equipos = Equipo,
                        Usuarios = user,
                        Fecha_Inicio = DateTime.Now,
                    };

                    _dataContext.Add(EquiUs);
                }
                else
                {
                    var EquiUs = new HistorialEquiUsEntity
                    { 
                        Id =equs.Id,
                        Equipos = equs.Equipos,
                        Usuarios = equs.Usuarios,
                        Fecha_Inicio = equs.Fecha_Inicio,
                        Fecha_Final = DateTime.Now                        
                    };

                    _dataContext.Update(EquiUs);
                }                                                            

                try
                {
                    await _dataContext.SaveChangesAsync();

                    /* var myToken = await _userHelper.GenerateEmailConfirmationTokenAsync(user);
                     var tokenLink = Url.Action("ConfirmEmail", "Account", new
                     {
                         userid = user.Id,
                         token = myToken
                     }, protocol: HttpContext.Request.Scheme);

                     _mailHelper.SendMail(model.Username, "Email confirmation", $"<h1>Email Confirmation</h1>" +
                         $"To allow the user, " +
                         $"plase click in this link:</br></br><a href = \"{tokenLink}\">Confirm Email</a>");
                         */
                    return RedirectToAction(nameof(Index));
                }
                catch (Exception ex)
                {
                    ModelState.AddModelError(string.Empty, ex.ToString());
                    return View(model);
                }
            }
            return View(model);
        }

        // GET: Equipos/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var equipoEntity = await (_dataContext.Equipos.Include(o => o.Sedes).Include(o => o.Tipos)).Where
                (o => o.Id == id).FirstOrDefaultAsync();
            if (equipoEntity == null)
            {
                return NotFound();
            }

            var equs = _dataContext.HistorialEquiUsEntity.FirstOrDefault(o => o.Equipos.Id == equipoEntity.Id
                && o.Fecha_Final == DateTime.MinValue);

            var model = new EquiposViewModel
            {
                Codigo = equipoEntity.Codigo,
                AspNetSedeId = equipoEntity.Sedes.Id.ToString(),
                AspNetTypeId = equipoEntity.Tipos.Id.ToString(),
                AspNetUsuarioId = equs.Id.ToString(),
                AspNetTypes = _combosHelper.GetComboTipoEquipos("Equipos"),
                AspNetUsuarios = _combosHelper.GetComboUsuarios(),
                AspNetSedes = _combosHelper.GetComboSedes(),
                Description = equipoEntity.Description,
                DescriptionDetallada = equipoEntity.DescriptionDetallada,
                Fecha_Compra = equipoEntity.Fecha_Compra,
                Marca = equipoEntity.Marca,
                Serial = equipoEntity.Serial,
                Valor = equipoEntity.Valor,
                Id = equipoEntity.Id,
            };
            return View(model);

            //return View(equipoEntity);
        }

        // POST: Equipos/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("Id,Codigo,Marca,Description,DescriptionDetallada,Serial,Fecha_Compra,Valor,AspNetTypeId,AspNetUsuarioId,AspNetSedeId")] EquiposViewModel equipoEntity)
        {
            if (id != equipoEntity.Id)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {

                    var equs = (_dataContext.HistorialEquiUsEntity.Include(o=>o.Equipos).Include(o=>o.Usuarios)).FirstOrDefault(o => o.Equipos.Id == equipoEntity.Id
                        && o.Fecha_Final == DateTime.MinValue);

                    equs.Fecha_Final = DateTime.Now;

                    var tipo = _dataContext.Tipos.FirstOrDefault(
                    o => o.Id.ToString() == equipoEntity.AspNetTypeId);

                    var sede = _dataContext.Sedes.FirstOrDefault(
                        o => o.Id.ToString() == equipoEntity.AspNetSedeId);


                    var equipo = _dataContext.Equipos.FirstOrDefault(o => o.Id == equipoEntity.Id);
                    equipo.Codigo = equipoEntity.Codigo;
                    equipo.Description = equipoEntity.Description;
                    equipo.Description = equipoEntity.DescriptionDetallada;
                    equipo.Fecha_Compra = equipoEntity.Fecha_Compra;
                    equipo.Marca = equipoEntity.Marca;
                    equipo.Sedes = sede;
                    equipo.Serial = equipoEntity.Serial;
                    equipo.Tipos = tipo;
                    equipo.Valor = equipoEntity.Valor;
                        
                    _dataContext.Update(equs);
                    _dataContext.Update(equipo);

                    var user = _dataContext.Usuarios.FirstOrDefault(o => o.Id.ToString() == equipoEntity.AspNetUsuarioId);
                    
                    var EquiUs = new HistorialEquiUsEntity
                    {
                        Equipos = equipo,
                        Usuarios = user,
                        Fecha_Inicio = DateTime.Now,
                    };

                    _dataContext.Add(EquiUs);

                    await _dataContext.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!EquipoEntityExists(equipoEntity.Id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            return View(equipoEntity);
        }

        // GET: Equipos/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var equipoEntity = await _dataContext.Equipos
                .FirstOrDefaultAsync(m => m.Id == id);
            if (equipoEntity == null)
            {
                return NotFound();
            }

            return View(equipoEntity);
        }

        // POST: Equipos/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var equipoEntity = await _dataContext.Equipos.FindAsync(id);
            _dataContext.Equipos.Remove(equipoEntity);
            await _dataContext.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool EquipoEntityExists(int id)
        {
            return _dataContext.Equipos.Any(e => e.Id == id);
        }
    }
}
