﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using sistemas.Web.Data;
using sistemas.Web.Data.Entities;
using sistemas.Web.Helpers;

namespace sistemas.Web.Controllers
{
    public class UsuariosController : Controller
    {
        private readonly DataContext _dataContext;
        private readonly ICombosHelper _combosHelper;

        public UsuariosController(DataContext context,
            ICombosHelper combosHelper)
        {
            _dataContext = context;
        }

        // GET: Usuarios
        public async Task<IActionResult> Index()
        {
            return View(await _dataContext.Usuarios.ToListAsync());
        }

        // GET: Usuarios/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var usuarioEntity = await _dataContext.Usuarios
                .FirstOrDefaultAsync(m => m.Id == id);
            if (usuarioEntity == null)
            {
                return NotFound();
            }

            return View(usuarioEntity);
        }

        // GET: Usuarios/Create
        public IActionResult Create()
        {
            return View();
        }

        // POST: Usuarios/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("Id,Cedula,Nombres,Apellidos,Cargo")] UsuarioEntity usuarioEntity)
        {
            if (ModelState.IsValid)
            {
                _dataContext.Add(usuarioEntity);
                await _dataContext.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            return View(usuarioEntity);
        }

        // GET: Usuarios/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var usuarioEntity = await _dataContext.Usuarios.FindAsync(id);
            if (usuarioEntity == null)
            {
                return NotFound();
            }
            return View(usuarioEntity);
        }

        // POST: Usuarios/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("Id,Cedula,Nombres,Apellidos,Cargo")] UsuarioEntity usuarioEntity)
        {
            if (id != usuarioEntity.Id)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _dataContext.Update(usuarioEntity);
                    await _dataContext.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!UsuarioEntityExists(usuarioEntity.Id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            return View(usuarioEntity);
        }

        // GET: Usuarios/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var usuarioEntity = await _dataContext.Usuarios
                .FirstOrDefaultAsync(m => m.Id == id);
            if (usuarioEntity == null)
            {
                return NotFound();
            }

            return View(usuarioEntity);
        }

        // POST: Usuarios/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var usuarioEntity = await _dataContext.Usuarios.FindAsync(id);
            _dataContext.Usuarios.Remove(usuarioEntity);
            await _dataContext.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool UsuarioEntityExists(int id)
        {
            return _dataContext.Usuarios.Any(e => e.Id == id);
        }
    }
}
