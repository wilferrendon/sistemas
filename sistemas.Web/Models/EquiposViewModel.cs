﻿using Microsoft.AspNetCore.Mvc.Rendering;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace sistemas.Web.Models
{
    public class EquiposViewModel
    {
        public int Id { get; set; }

        [Display(Name = "Codigo")]
        [Required(ErrorMessage = "El campo {0} es obligatorio.")]
        public int Codigo { get; set; }

        [Display(Name = "Marca")]
        [MaxLength(40, ErrorMessage = "El {0} campo no puede tener mas de {1} caracteres.")]
        [Required(ErrorMessage = "El campo {0} es obligatorio.")]

        public string Marca { get; set; }

        [Display(Name = "Descripcion")]
        [MaxLength(40, ErrorMessage = "El {0} campo no puede tener mas de {1} caracteres.")]
        [Required(ErrorMessage = "El campo {0} es obligatorio.")]


        public string Description { get; set; }

        [Display(Name = "Descripción Detallada")]

        public string DescriptionDetallada { get; set; }

        [Display(Name = "Serial")]

        public string Serial { get; set; }

        
        
        public DateTime Fecha_Compra { get; set; }

        [Display(Name = "Fecha Compra")]
        [Required(ErrorMessage = "El campo {0} es obligatorio.")]
        [DisplayFormat(DataFormatString = "{0:yyyy/MM/dd}", ApplyFormatInEditMode = true)]
        public DateTime Fecha_CompraLocal => Fecha_Compra.ToLocalTime();

        [Display(Name = "Valor")]
        [Required(ErrorMessage = "El campo {0} es obligatorio.")]
        [DisplayFormat(DataFormatString = "{0:C2}", ApplyFormatInEditMode = false)]
        public decimal Valor { get; set; }

        [Display(Name = "Tipo")]
        [Required(ErrorMessage = "El campo {0} es obligatorio.")]
        public string AspNetTypeId { get; set; }
        public IEnumerable<SelectListItem> AspNetTypes { get; set; }

        [Display(Name = "Usuario")]
        public string AspNetUsuarioId { get; set; }
        public IEnumerable<SelectListItem> AspNetUsuarios { get; set; }

        [Display(Name = "Sedes")]
        public string AspNetSedeId { get; set; }
        public IEnumerable<SelectListItem> AspNetSedes { get; set; }
    }
}
