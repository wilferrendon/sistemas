﻿using Microsoft.AspNetCore.Mvc.Rendering;
using sistemas.Web.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace sistemas.Web.Helpers
{
    public class CombosHelper : ICombosHelper
    {
        private readonly DataContext _dataContext;

        public CombosHelper(DataContext dataContext)
        {
            _dataContext = dataContext;
        }
        public IEnumerable<SelectListItem> GetComboTipoEquipos(string opcion)
        {
            var list = _dataContext.Tipos.Where(e => e.Tipo_Id == opcion)
                .Select(pt => new SelectListItem
                {
                    Text = pt.Description,
                    Value = $"{pt.Id}"
                })
                .OrderBy(pt => pt.Text)
                .ToList();

            list.Insert(0, new SelectListItem
            {
                Text = "[Seleccione un estado...]",
                Value = "0"
            });

            return list;
        }

        public IEnumerable<SelectListItem> GetComboUsuarios()
        {
            var list = _dataContext.Usuarios
                .Select(pt => new SelectListItem
                {
                    Text = pt.Nombres+" "+pt.Apellidos,
                    Value = $"{pt.Id}"
                })
                .OrderBy(pt => pt.Text)
                .ToList();

            list.Insert(0, new SelectListItem
            {
                Text = "[Seleccione un usuario...]",
                Value = "0"
            });

            return list;
        }

        public IEnumerable<SelectListItem> GetComboSedes()
        {
            var list = _dataContext.Sedes
                .Select(pt => new SelectListItem
                {
                    Text = pt.Description,
                    Value = $"{pt.Id}"
                })
                .OrderBy(pt => pt.Text)
                .ToList();

            list.Insert(0, new SelectListItem
            {
                Text = "[Seleccione una sede...]",
                Value = "0"
            });

            return list;
        }


    }
}
