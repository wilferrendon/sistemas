﻿using Microsoft.AspNetCore.Mvc.Rendering;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace sistemas.Web.Helpers
{
    public interface ICombosHelper
    {
        IEnumerable<SelectListItem> GetComboTipoEquipos(string opcion);

        IEnumerable<SelectListItem> GetComboUsuarios();

        IEnumerable<SelectListItem> GetComboSedes();

    }
}
