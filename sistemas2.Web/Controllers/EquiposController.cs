﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using sistemas2.Web.Data;
using sistemas2.Web.Data.Entities;
using sistemas2.Web.Helpers;
using sistemas2.Web.Models;

namespace sistemas2.Web.Controllers
{
    public class EquiposController : Controller
    {
        private readonly DataContext _dataContext;
        private readonly ICombosHelper _combosHelper;
        private readonly IConverterHelper _converterHelper;
        private readonly IImageHelper _imageHelper;

        public EquiposController(DataContext context,
            ICombosHelper combosHelper, IConverterHelper converterHelper,IImageHelper imageHelper)
        {
            _dataContext = context;
            _combosHelper = combosHelper;
            _converterHelper = converterHelper;
            _imageHelper = imageHelper;
        }

        // GET: Equipos
        // GET: Equipos
        public async Task<IActionResult> Index()
        {
            // return View(await _dataContext.Equipos.Include(o => o.Tipos).ToListAsync());  
            // await Task.Delay(TimeSpan.FromSeconds(5));

            DateTime date = DateTime.MinValue;

            var query = from u in _dataContext.Equipos
                        join t in _dataContext.Tipos on u.TipoEntity.Id equals t.Id
                        //join z in _dataContext.HistorialEquiUsEntity on u.Description equals z.Equipo
                        //where z.Fecha_Final == date
                        select new EquiposViewModel
                        {
                            Id = u.Id,
                            Codigo = u.Codigo,
                            Description = u.Description,
                            DescriptionDetallada = u.DescriptionDetallada,
                            Fecha_Compra = u.Fecha_Compra,
                            Marca = u.Marca,
                            //AspNetUsuarioId = z.Usuario,
                            Serial = u.Serial,
                            Valor = u.Valor,
                            InvoicePath = u.InvoicePath,
                            EquipoPath = u.EquipoPath,
                            AspNetTypeId = u.TipoEntity.Description.ToString(),
                        };

            return View(query.OrderBy(o => o.Id));
        }

        // GET: Equipos/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var equipoEntity = await _dataContext.Equipos
                .Include(o => o.EqSoftwares).ThenInclude(o=>o.SoftwareEntity)
                .Include(o => o.EqHardwares).ThenInclude(o => o.HardwareEntity)
                .FirstOrDefaultAsync(m => m.Id == id);
            if (equipoEntity == null)
            {
                return NotFound();
            }

            return View(equipoEntity);
        }

        // GET: Equipos/Create
        public IActionResult Create()
        {
            var max = _dataContext.Equipos.Count();

            var model = new EquiposViewModel
            {
                Codigo = Convert.ToInt32(max),
                AspNetTypes = _combosHelper.GetComboTipoEquipos("Equipos"),
                AspNetUsuarios = _combosHelper.GetComboUsuarios(),
                AspNetSedes = _combosHelper.GetComboSedes(),
            };
            return View(model);
        }

        // POST: Equipos/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create(EquiposViewModel model)
        {
            if (ModelState.IsValid)
            {
                var invoicePath = string.Empty;
                var equipoPath = string.Empty;

                if(model.InvoiceFile != null)
                {
                    invoicePath = await _imageHelper.UploadImageAsync(model.InvoiceFile, "Equipos");
                }

                if (model.EquipoFile != null)
                {
                    equipoPath = await _imageHelper.UploadImageAsync(model.EquipoFile, "Equipos");
                }

                var tipo = _dataContext.Tipos.FirstOrDefault(
                    o => o.Id.ToString() == model.AspNetTypeId);

                var sede = _dataContext.Sedes.FirstOrDefault(
                    o => o.Id.ToString() == model.AspNetSedeId);

                var user = _dataContext.Usuarios.FirstOrDefault(o => o.Id.ToString() == model.AspNetUsuarioId);

                var Equipo = new EquipoEntity
                {
                    Codigo = model.Codigo,
                    Description = model.Description,
                    DescriptionDetallada = model.DescriptionDetallada,
                    Fecha_Compra = model.Fecha_Compra,
                    Marca = model.Marca,
                    Serial = model.Serial,
                    TipoEntity = tipo,
                    Valor = model.Valor,
                    InvoicePath = invoicePath,
                    EquipoPath = equipoPath,
                    SedeEntity = sede,
                    UsuarioEntity = user,
                };

                _dataContext.Add(Equipo);
                              
                var equs = _dataContext.HistorialEquiUsEntity.FirstOrDefault(o => o.Equipo == Equipo.Description
                && o.Usuario == user.Nombres + " " + user.Apellidos);

                if (equs == null)
                {
                    var EquiUs = new HistorialEquiUsEntity
                    {
                        Equipo = Equipo.Description,
                        Usuario = user.Nombres + " " + user.Apellidos,
                        Fecha_Inicio = DateTime.Now,
                    };

                    _dataContext.Add(EquiUs);
                }
                else
                {
                    var EquiUs = new HistorialEquiUsEntity
                    {
                        Id = equs.Id,
                        Equipo = equs.Equipo,
                        Usuario = equs.Usuario,
                        Fecha_Inicio = equs.Fecha_Inicio,
                        Fecha_Final = DateTime.Now
                    };

                    _dataContext.Update(EquiUs);
                }

                try
                {
                    await _dataContext.SaveChangesAsync();

                    /* var myToken = await _userHelper.GenerateEmailConfirmationTokenAsync(user);
                     var tokenLink = Url.Action("ConfirmEmail", "Account", new
                     {
                         userid = user.Id,
                         token = myToken
                     }, protocol: HttpContext.Request.Scheme);

                     _mailHelper.SendMail(model.Username, "Email confirmation", $"<h1>Email Confirmation</h1>" +
                         $"To allow the user, " +
                         $"plase click in this link:</br></br><a href = \"{tokenLink}\">Confirm Email</a>");
                         */
                    return RedirectToAction(nameof(Index));
                }
                catch (Exception ex)
                {
                    ModelState.AddModelError(string.Empty, ex.ToString());
                    return View(model);
                }
            }
            return View(model);
        }

        // GET: Equipos/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var equipoEntity = await _dataContext.Equipos.Include(o => o.TipoEntity)
                .FirstOrDefaultAsync(m => m.Id == id);

            if (equipoEntity == null)
            {
                return NotFound();
            }

            EquiposViewModel model = new EquiposViewModel
            {
                Id = equipoEntity.Id,
                Codigo = equipoEntity.Codigo,
                Description = equipoEntity.Description,
                DescriptionDetallada = equipoEntity.DescriptionDetallada,
                AspNetSedes = _combosHelper.GetComboSedes(),
                AspNetTypes = _combosHelper.GetComboTipoEquipos("Equipos"),
                AspNetTypeId = equipoEntity.TipoEntity.Id.ToString(),
                EquipoPath = equipoEntity.EquipoPath,
                InvoicePath = equipoEntity.InvoicePath,
                Fecha_Compra = equipoEntity.Fecha_Compra,
                Marca = equipoEntity.Marca,
                Serial = equipoEntity.Serial,
                Valor = equipoEntity.Valor,

            };



            return View(model);
        }

        // POST: Equipos/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id,string InvoicePath, string EquipoPath, EquiposViewModel model)
        {
            if (id != model.Id)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    
                    var invoicePath = InvoicePath;
                    var equipoPath = EquipoPath;

                    if (model.InvoiceFile != null)
                    {
                        invoicePath = await _imageHelper.UploadImageAsync(model.InvoiceFile, "Equipos");
                    }

                    if (model.EquipoFile != null)
                    {
                        equipoPath = await _imageHelper.UploadImageAsync(model.EquipoFile, "Equipos");
                    }

                    EquipoEntity equipoEntity = new EquipoEntity
                    {
                        Id = model.Id,
                        Codigo = model.Codigo,
                        Description = model.Description,
                        DescriptionDetallada = model.DescriptionDetallada,
                        EquipoPath =equipoPath,
                        InvoicePath = invoicePath,
                        Fecha_Compra = model.Fecha_Compra,
                        Marca = model.Marca,
                        Serial = model.Serial,
                        Valor = model.Valor,  
                    };                    

                    _dataContext.Update(equipoEntity);
                    await _dataContext.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!EquipoEntityExists(model.Id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            return View(model);
        }

        // GET: Equipos/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var equipoEntity = await _dataContext.Equipos
                .FirstOrDefaultAsync(m => m.Id == id);
            if (equipoEntity == null)
            {
                return NotFound();
            }

            return View(equipoEntity);
        }

        // POST: Equipos/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var equipoEntity = await _dataContext.Equipos.FindAsync(id);
            _dataContext.Equipos.Remove(equipoEntity);
            await _dataContext.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool EquipoEntityExists(int id)
        {
            return _dataContext.Equipos.Any(e => e.Id == id);
        }

        public async Task<IActionResult> AddSoftwares(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var equipoEntity = await _dataContext.Equipos
                .FirstOrDefaultAsync(m => m.Id ==id);

            if (equipoEntity == null)
            {
                return NotFound();
            }

            EquipoSoftwaresViewModel model = new EquipoSoftwaresViewModel
            {
                EquipoId = equipoEntity.Id,
                Description = equipoEntity.Description,                
                AspNetSoftwares = _combosHelper.GetComboSoftwares(),
            };

            return View(model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> AddSoftwares(int EquipoId, EquipoSoftwaresViewModel model)
        {
            if (ModelState.IsValid)
            {
                var equipoEntity = await _dataContext.Equipos
                .FirstOrDefaultAsync(m => m.Id == EquipoId);

                if (equipoEntity == null)
                {
                    return NotFound();
                }

                var softwareEntity = _dataContext.Softwares.FirstOrDefault(
                    o => o.Id.ToString() == model.AspNetSoftwareId);

                var equipoSoftware = new EquipoSoftwareEntity
                {
                    EquipoEntity = equipoEntity,
                    SoftwareEntity = softwareEntity,
                };

                _dataContext.Add(equipoSoftware);

                await _dataContext.SaveChangesAsync();

               

            }

            return RedirectToAction("Details", new { id = EquipoId });
        }

        public async Task<IActionResult> AddHardwares(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var equipoEntity = await _dataContext.Equipos
                .FirstOrDefaultAsync(m => m.Id == id);

            if (equipoEntity == null)
            {
                return NotFound();
            }

            EquipoHardwaresViewModel model = new EquipoHardwaresViewModel
            {
                EquipoId = equipoEntity.Id,
                Description = equipoEntity.Description,
                AspNetHardwares = _combosHelper.GetComboHardwares(),
            };

            return View(model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> AddHardwares(int EquipoId, EquipoHardwaresViewModel model)
        {
            if (ModelState.IsValid)
            {
                var equipoEntity = await _dataContext.Equipos
                 .FirstOrDefaultAsync(m => m.Id == EquipoId);

                if (equipoEntity == null)
                {
                    return NotFound();
                }

                var hardwareEntity = _dataContext.Hardwares.FirstOrDefault(
                    o => o.Id.ToString() == model.AspNetHardwareId);

                var equipoHardware = new EquipoHarwareEntity
                {
                    EquipoEntity = equipoEntity,
                    HardwareEntity = hardwareEntity,
                };

                _dataContext.Add(equipoHardware);

                await _dataContext.SaveChangesAsync();

            }

            return RedirectToAction("Details", new { id = EquipoId });
        }

        public async Task<IActionResult> DelSoftwares(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var equipoEntity = await _dataContext.EquipoSoftwares.Include(o => o.SoftwareEntity).Include(o => o.EquipoEntity)
                .FirstOrDefaultAsync(m => m.Id == id);

            if (equipoEntity == null)
            {
                return NotFound();
            }

            return View(equipoEntity);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DelSoftwaresConfirmed(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            EquipoSoftwareEntity softwareEntity = await _dataContext.EquipoSoftwares.Include(o => o.EquipoEntity)
                .FirstOrDefaultAsync(m => m.Id == id);
            if (softwareEntity == null)
            {
                return NotFound();
            }

            _dataContext.EquipoSoftwares.Remove(softwareEntity);
            await _dataContext.SaveChangesAsync();
            return RedirectToAction($"{nameof(Details)}/{softwareEntity.EquipoEntity.Id}");
        }


        public async Task<IActionResult> DelHardwares(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var equipoEntity = await _dataContext.EquipoHardwares.Include(o => o.HardwareEntity).Include(o => o.EquipoEntity)
                .FirstOrDefaultAsync(m => m.Id == id);

            if (equipoEntity == null)
            {
                return NotFound();
            }

            return View(equipoEntity);
        }


        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DelHardwaresConfirmed(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            EquipoHarwareEntity hardwareEntity = await _dataContext.EquipoHardwares.Include(o => o.EquipoEntity)
                .FirstOrDefaultAsync(m => m.Id == id);


            if (hardwareEntity == null)
            {
                return NotFound();
            }

            _dataContext.EquipoHardwares.Remove(hardwareEntity);
            await _dataContext.SaveChangesAsync();
            return RedirectToAction("Details", new { id = hardwareEntity.EquipoEntity.Id });
        }
    }
}
