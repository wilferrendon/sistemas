﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using sistemas2.Web.Data;
using sistemas2.Web.Data.Entities;
using sistemas2.Web.Helpers;
using sistemas2.Web.Models;

namespace sistemas2.Web.Controllers
{
    public class HardwaresController : Controller
    {
        private readonly DataContext _dataContext;
        private readonly ICombosHelper _combosHelper;

        public HardwaresController(DataContext context,
            ICombosHelper combosHelper)
        {
            _dataContext = context;
            _combosHelper = combosHelper;
        }

        // GET: Hardwares
        public async Task<IActionResult> Index()
        {

            var query = from u in _dataContext.Hardwares.Include(o => o.TipoEntity)
                        select new HardwaresViewModel
                        {
                            Id = u.Id,
                            Description = u.Description,
                            Serial = u.Serial,
                            AspNetTypeId = u.TipoEntity.Description.ToString(),
                        };

            return View(query.OrderBy(o => o.Id));

        }

        // GET: Hardwares/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var hardwareEntity = await _dataContext.Hardwares
                .FirstOrDefaultAsync(m => m.Id == id);
            if (hardwareEntity == null)
            {
                return NotFound();
            }

            return View(hardwareEntity);
        }

        // GET: Hardwares/Create
        public IActionResult Create()
        {

            var model = new HardwaresViewModel
            {
                AspNetTypes = _combosHelper.GetComboTipoEquipos("Hardware"),
            };
            return View(model);

        }

        // POST: Hardwares/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create(HardwaresViewModel model)
        {
            if (ModelState.IsValid)
            {
                var tipo = _dataContext.Tipos.FirstOrDefault(
                    o => o.Id.ToString() == model.AspNetTypeId);


                var Hardware = new HardwareEntity
                {
                    Description = model.Description,
                    Serial = model.Serial,
                    TipoEntity = tipo,
                };

                _dataContext.Add(Hardware);

                try
                {
                    await _dataContext.SaveChangesAsync();
                    return RedirectToAction(nameof(Index));
                }
                catch (Exception ex)
                {
                    ModelState.AddModelError(string.Empty, ex.ToString());
                    return View(model);
                }
            }

            return View(model);
        }

        // GET: Hardwares/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var hardwareEntity = await (_dataContext.Hardwares.Include(f => f.TipoEntity)).Where
               (o => o.Id == id).FirstOrDefaultAsync();
            if (hardwareEntity == null)
            {
                return NotFound();
            }

            var model = new HardwaresViewModel
            {
                Description = hardwareEntity.Description,
                Serial = hardwareEntity.Serial,
                AspNetTypeId = hardwareEntity.TipoEntity.Id.ToString(),
                AspNetTypes = _combosHelper.GetComboTipoEquipos("Hardware"),
            };

            return View(model);
        }

        // POST: Hardwares/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, HardwaresViewModel hardwareEntity)
        {
            if (id != hardwareEntity.Id)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {

                    var tipo = _dataContext.Tipos.FirstOrDefault(
                    o => o.Id.ToString() == hardwareEntity.AspNetTypeId);

                    var hardware = _dataContext.Hardwares.FirstOrDefault(o => o.Id == hardwareEntity.Id);
                    hardware.TipoEntity = tipo;

                    _dataContext.Update(hardware);
                    await _dataContext.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!HardwareEntityExists(hardwareEntity.Id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            return View(hardwareEntity);
        }

        // GET: Hardwares/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var hardwareEntity = await _dataContext.Hardwares
                .FirstOrDefaultAsync(m => m.Id == id);
            if (hardwareEntity == null)
            {
                return NotFound();
            }

            return View(hardwareEntity);
        }

        // POST: Hardwares/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var hardwareEntity = await _dataContext.Hardwares.FindAsync(id);
            _dataContext.Hardwares.Remove(hardwareEntity);
            await _dataContext.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool HardwareEntityExists(int id)
        {
            return _dataContext.Hardwares.Any(e => e.Id == id);
        }
    }
}
