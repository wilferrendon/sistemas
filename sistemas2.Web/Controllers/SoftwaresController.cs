﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using sistemas2.Web.Data;
using sistemas2.Web.Data.Entities;
using sistemas2.Web.Helpers;
using sistemas2.Web.Models;

namespace sistemas2.Web.Controllers
{
    public class SoftwaresController : Controller
    {
        private readonly DataContext _dataContext;
        private readonly ICombosHelper _combosHelper;

        public SoftwaresController(DataContext context,
            ICombosHelper combosHelper)
        {
            _dataContext = context;
            _combosHelper = combosHelper;
        }

        // GET: Softwares
        public async Task<IActionResult> Index()
        {
            var query = from u in _dataContext.Softwares.Include(o => o.TipoEntity)
                        select new SoftwaresViewModel
                        {
                            Id = u.Id,
                            Description = u.Description,
                            Serial = u.Serial,
                            AspNetTypeId = u.TipoEntity.Description.ToString(),
                        };

            return View(query.OrderBy(o => o.Id));
        }

        // GET: Softwares/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var softwareEntity = await _dataContext.Softwares
                .FirstOrDefaultAsync(m => m.Id == id);
            if (softwareEntity == null)
            {
                return NotFound();
            }

            return View(softwareEntity);
        }

        // GET: Softwares/Create
        public IActionResult Create()
        {
            var model = new SoftwaresViewModel
            {
                AspNetTypes = _combosHelper.GetComboTipoEquipos("Software"),
            };
            return View(model);
        }

        // POST: Softwares/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create(SoftwaresViewModel model)
        {
            if (ModelState.IsValid)
            {
                var tipo = _dataContext.Tipos.FirstOrDefault(
                    o => o.Id.ToString() == model.AspNetTypeId);


                var Software = new SoftwareEntity
                {
                    Description = model.Description,
                    Serial = model.Serial,
                    TipoEntity = tipo,
                };

                _dataContext.Add(Software);

                try
                {
                    await _dataContext.SaveChangesAsync();
                    return RedirectToAction(nameof(Index));
                }
                catch (Exception ex)
                {
                    ModelState.AddModelError(string.Empty, ex.ToString());
                    return View(model);
                }
            }

            return View(model);
        }

        // GET: Softwares/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var softwareEntity = await (_dataContext.Softwares.Include(f => f.TipoEntity)).Where
               (o => o.Id == id).FirstOrDefaultAsync();
            if (softwareEntity == null)
            {
                return NotFound();
            }

            var model = new SoftwaresViewModel
            {
                Description = softwareEntity.Description,
                Serial = softwareEntity.Serial,
                DescriptionDetallada = softwareEntity.DescriptionDetallada,
                AspNetTypeId = softwareEntity.TipoEntity.Id.ToString(),
                AspNetTypes = _combosHelper.GetComboTipoEquipos("Software"),
            };

            return View(model);
        }

        // POST: Softwares/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, HardwaresViewModel softwareEntity)
        {
            if (id != softwareEntity.Id)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {

                    var tipo = _dataContext.Tipos.FirstOrDefault(
                    o => o.Id.ToString() == softwareEntity.AspNetTypeId);

                    var software = _dataContext.Softwares.FirstOrDefault(o => o.Id == softwareEntity.Id);
                    software.TipoEntity = tipo;

                    _dataContext.Update(software);
                    await _dataContext.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!SoftwareEntityExists(softwareEntity.Id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            return View(softwareEntity);
        }

        // GET: Softwares/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var softwareEntity = await _dataContext.Softwares
                .FirstOrDefaultAsync(m => m.Id == id);
            if (softwareEntity == null)
            {
                return NotFound();
            }

            return View(softwareEntity);
        }

        // POST: Softwares/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var softwareEntity = await _dataContext.Softwares.FindAsync(id);
            _dataContext.Softwares.Remove(softwareEntity);
            await _dataContext.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool SoftwareEntityExists(int id)
        {
            return _dataContext.Softwares.Any(e => e.Id == id);
        }
    }
}
