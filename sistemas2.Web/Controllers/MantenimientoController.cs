﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using sistemas2.Web.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace sistemas2.Web.Controllers
{
    public class MantenimientoController : Controller
    {
        private readonly DataContext _context;

        public MantenimientoController(DataContext context)
        {
            _context = context;
        }
    
        public async Task<IActionResult> Index()
        {
            return View(await _context.MantenPreventivo.ToListAsync());
        }
    }
}
