﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using sistemas2.Web.Data;
using sistemas2.Web.Data.Entities;
using sistemas2.Web.Helpers;
using sistemas2.Web.Models;

namespace sistemas2.Web.Controllers
{
    public class TrasladoEntitiesController : Controller
    {
        private readonly DataContext _dataContext;
        private readonly ICombosHelper _combosHelper;

        public TrasladoEntitiesController(DataContext context,
            ICombosHelper combosHelper)
        {
            _dataContext = context;
            _combosHelper = combosHelper;
        }

        // GET: TrasladoEntities
        public async Task<IActionResult> Index()
        {
            return View(await _dataContext.Traslados.ToListAsync());
        }

        // GET: TrasladoEntities/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

           

            var equipoEntity = await _dataContext.Traslados
                .Include(o => o.TrasladosDetalles).ThenInclude(o => o.EquipoEntity)
                .FirstOrDefaultAsync(m => m.Id == id);
            if (equipoEntity == null)
            {
                return NotFound();
            }

            return View(equipoEntity);
        }

        // GET: TrasladoEntities/Create
        public IActionResult Create()
        {
            var max = _dataContext.Traslados.Count();

            var model = new TrasladosViewModel
            {
                FechaTraslado = DateTime.Now,
                Traslado = max.ToString(),
                AspNetSedes = _combosHelper.GetComboSedes(),
                AspNetSedes2 = _combosHelper.GetComboSedes(),
                AspNetUsuarios = _combosHelper.GetComboUsuarios(),
                AspNetUsuarios2 = _combosHelper.GetComboUsuarios(),
                
            };
            return View(model);
        }

        // POST: TrasladoEntities/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create(TrasladosViewModel model)
        {
            if (ModelState.IsValid)
            {
                var sedeOrigen = _dataContext.Sedes.FirstOrDefault(
                    o => o.Id.ToString() == model.AspNetSedesOrigenId);

                var sedeDestino = _dataContext.Sedes.FirstOrDefault(
                   o => o.Id.ToString() == model.AspNetSedesDestinoId);

                var userOrigen = _dataContext.Usuarios.FirstOrDefault(o => o.Id.ToString() == model.AspNetUsuariosOrigenId);
                var userDestino = _dataContext.Usuarios.FirstOrDefault(o => o.Id.ToString() == model.AspNetUsuariosDestinoId);

                var Traslado = new TrasladoEntity
                {
                    Id = model.Id,
                    FechaTraslado = model.FechaTraslado,
                    Last_Updated = DateTime.Now,
                    Last_User_By = "ADMIN",
                    SedeOrigen = sedeOrigen.Description,
                    SedeDestino = sedeDestino.Description,
                    Status = "Activo",
                    Traslado = model.Traslado,
                    UsuarioOrigen = userOrigen.Cedula,
                    UsuarioDestino = userDestino.Cedula,
                    Motivo = model.Motivo,
                };

                _dataContext.Add(Traslado);


                try
                {
                    await _dataContext.SaveChangesAsync();

                    return RedirectToAction(nameof(Index));
                }
                catch (Exception ex)
                {
                    ModelState.AddModelError(string.Empty, ex.ToString());
                    return View(model);
                }
            }
            return View(model);
        }

        // GET: TrasladoEntities/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var equipoEntity = await _dataContext.Traslados
                .FirstOrDefaultAsync(m => m.Id == id);

            if (equipoEntity == null)
            {
                return NotFound();
            }

            var sedeOrigen = _dataContext.Sedes.FirstOrDefault(
                    o => o.Description.ToString() == equipoEntity.SedeOrigen);

            var sedeDestino = _dataContext.Sedes.FirstOrDefault(
               o => o.Description.ToString() == equipoEntity.SedeDestino);

            var userOrigen = _dataContext.Usuarios.FirstOrDefault(o => o.Cedula.ToString() == equipoEntity.UsuarioOrigen.ToString());
            var userDestino = _dataContext.Usuarios.FirstOrDefault(o => o.Cedula.ToString() == equipoEntity.UsuarioDestino.ToString());

            var Traslado = new TrasladosViewModel
            {
                Id = equipoEntity.Id,
                FechaTraslado = equipoEntity.FechaTraslado,
                AspNetSedesOrigenId = sedeOrigen.Id.ToString(),
                AspNetSedesDestinoId = sedeDestino.Id.ToString(),
                Traslado = equipoEntity.Traslado,
                AspNetUsuariosOrigenId = userOrigen.Id.ToString(),
                AspNetUsuariosDestinoId = userDestino.Id.ToString(),
                Motivo = equipoEntity.Motivo,
                AspNetSedes = _combosHelper.GetComboSedes(),
                AspNetSedes2 = _combosHelper.GetComboSedes(),
                AspNetUsuarios = _combosHelper.GetComboUsuarios(),
                AspNetUsuarios2 = _combosHelper.GetComboUsuarios(),
            };

            return View(Traslado);
        }

        // POST: TrasladoEntities/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id,  TrasladosViewModel model)
        {
            if (id != model.Id)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {

                    var sedeOrigen = _dataContext.Sedes.FirstOrDefault(
                    o => o.Id.ToString() == model.AspNetSedesOrigenId);

                    var sedeDestino = _dataContext.Sedes.FirstOrDefault(
                       o => o.Id.ToString() == model.AspNetSedesDestinoId);

                    var userOrigen = _dataContext.Usuarios.FirstOrDefault(o => o.Id.ToString() == model.AspNetUsuariosOrigenId);
                    var userDestino = _dataContext.Usuarios.FirstOrDefault(o => o.Id.ToString() == model.AspNetUsuariosDestinoId);

                    var Traslado = new TrasladoEntity
                    {
                        Id = model.Id,
                        Last_Updated = DateTime.Now,
                        Last_User_By = "ADMIN",
                        SedeOrigen = sedeOrigen.Description,
                        SedeDestino = sedeDestino.Description,
                        Status = "Activo",
                        Traslado = model.Traslado,
                        UsuarioOrigen = userOrigen.Cedula,
                        UsuarioDestino = userDestino.Cedula,
                        Motivo = model.Motivo,
                    };

                    _dataContext.Update(Traslado);
                    await _dataContext.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!TrasladoEntityExists(model.Id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            return View(model);
        }

        // GET: TrasladoEntities/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }


            var trasladoEntity = await _dataContext.Traslados
                .FirstOrDefaultAsync(m => m.Id == id);
            if (trasladoEntity == null)
            {
                return NotFound();
            }

            return View(trasladoEntity);
        }

        // POST: TrasladoEntities/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var trasladoEntity = await _dataContext.Traslados.FindAsync(id);

            var trasladoDetEntity = await _dataContext.TrasladosDetalles
                .FirstOrDefaultAsync(m => m.TrasladoEntity.Traslado == trasladoEntity.Traslado);
            
            if(trasladoDetEntity != null)
                _dataContext.TrasladosDetalles.Remove(trasladoDetEntity);

            _dataContext.Traslados.Remove(trasladoEntity);
            await _dataContext.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool TrasladoEntityExists(int id)
        {
            return _dataContext.Traslados.Any(e => e.Id == id);
        }

        public async Task<IActionResult> AddTrasladoDet(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var max = _dataContext.TrasladosDetalles.Count();

            var trasladoEntity = await _dataContext.Traslados
                .FirstOrDefaultAsync(m => m.Id == id);

            if (trasladoEntity == null)
            {
                return NotFound();
            }

            TrasladoDetallesViewModel model = new TrasladoDetallesViewModel
            {
                TrasladoId = trasladoEntity.Id,
                Consecutivo = max.ToString(),
                AspNetEquipos = _combosHelper.GetComboEquipos(),
            };

            return View(model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> AddTrasladoDet(int TrasladoId, TrasladoDetallesViewModel model)
        {
            if (ModelState.IsValid)
            {
                var trasladoEntity = await _dataContext.Traslados
                .FirstOrDefaultAsync(m => m.Id == TrasladoId);

                if (trasladoEntity == null)
                {
                    return NotFound();
                }

                var equipoEntity = _dataContext.Equipos.FirstOrDefault(
                    o => o.Id.ToString() == model.AspNetEquipoId);

                var trasladoDetalle = new TrasladoDetalleEntity
                {
                    TrasladoEntity = trasladoEntity,
                    Consecutivo = model.Consecutivo,
                    EquipoEntity = equipoEntity,
                };

                _dataContext.Add(trasladoDetalle);

                await _dataContext.SaveChangesAsync();

            }

            return RedirectToAction("Details", new { id = TrasladoId });
        }

        public async Task<IActionResult> DeleteTrasladosDet(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            TrasladoDetalleEntity trasladoDetalle = await _dataContext.TrasladosDetalles.Include(o => o.TrasladoEntity)
                .FirstOrDefaultAsync(m => m.Id == id);
            if (trasladoDetalle == null)
            {
                return NotFound();
            }

            _dataContext.TrasladosDetalles.Remove(trasladoDetalle);
            await _dataContext.SaveChangesAsync();
            return RedirectToAction("Details", new { id = trasladoDetalle.TrasladoEntity.Id });
        }

        public async Task<IActionResult> ReceiveTraslado(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var trasladoEntity = await _dataContext.Traslados
                .FirstOrDefaultAsync(m => m.Id == id);
            if (trasladoEntity == null)
            {
                return NotFound();
            }

            if (trasladoEntity.Status == "Activo")
            {

                trasladoEntity.FechaRecibido = DateTime.Now;
                trasladoEntity.Last_Updated = DateTime.Now;
                trasladoEntity.Status = "Recibido";
            }
            else
            {

            }

            _dataContext.Update(trasladoEntity);
            await _dataContext.SaveChangesAsync();
            return RedirectToAction("Index");
        }

    }
}
