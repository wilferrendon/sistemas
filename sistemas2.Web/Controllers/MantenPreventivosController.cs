﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using sistemas2.Web.Data;
using sistemas2.Web.Data.Entities;
using sistemas2.Web.Helpers;
using sistemas2.Web.Models;

namespace sistemas2.Web.Controllers
{
    public class MantenPreventivosController : Controller
    {
        private readonly DataContext _dataContext;
        private readonly ICombosHelper _combosHelper;

        public MantenPreventivosController(DataContext context,
            ICombosHelper combosHelper)
        {
            _dataContext = context;
            _combosHelper = combosHelper;
        }

        // GET: MantenPreventivos
        public async Task<IActionResult> Index()
        {
            return View(await _dataContext.MantenPreventivo.Include(o=>o.EquipoEntity).Include(o=>o.UsuarioEntity).ToListAsync());
        }

        // GET: MantenPreventivos/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var mantenPreventivo = await _dataContext.MantenPreventivo
                .FirstOrDefaultAsync(m => m.Id == id);
            if (mantenPreventivo == null)
            {
                return NotFound();
            }

            return View(mantenPreventivo);
        }

        // GET: MantenPreventivos/Create
        public IActionResult Create()
        {
            var max = _dataContext.MantenPreventivo.Count();

            var model = new MantenPreventivosViewModel
            {
                MantenPreventivoId = Convert.ToInt32(max),
                AspNetUsuarios = _combosHelper.GetComboUsuarios(),
                AspNetEquipos = _combosHelper.GetComboEquipos(),
                AspNetFrecuencias = _combosHelper.GetComboFrecuencias(),
                Status = "Activo",
            };
            return View(model);
        }

        // POST: MantenPreventivos/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create(MantenPreventivosViewModel model)
        {
            if (ModelState.IsValid)
            {

                var equipo = _dataContext.Equipos.FirstOrDefault(o => o.Id.ToString() == model.AspNetEquipoId);
                var user = _dataContext.Usuarios.FirstOrDefault(o => o.Id.ToString() == model.AspNetUsuariosId);

                var mantenPreventivo = new MantenPreventivo
                {
                    Id = model.Id,
                    EquipoEntity = equipo,
                    MantenPreventivoId = model.MantenPreventivoId,
                    Observaciones = model.Observaciones,
                    Dia = model.Dia,
                    Frecuencia = model.AspNetFrecuenciaId,
                    Status = "Activo",
                    UsuarioEntity = user,
                };

                _dataContext.Add(mantenPreventivo);

                try
                {
                    await _dataContext.SaveChangesAsync();

                    return RedirectToAction(nameof(Index));
                }
                catch (Exception ex)
                {
                    ModelState.AddModelError(string.Empty, ex.ToString());
                    return View(model);
                }
            }
            return View(model);
        }

        // GET: MantenPreventivos/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var mantenPreventivo = await _dataContext.MantenPreventivo.Include(o => o.EquipoEntity).Include(o => o.UsuarioEntity)
                .FirstOrDefaultAsync(m => m.Id == id);

            if (mantenPreventivo == null)
            {
                return NotFound();
            }

            var model = new MantenPreventivosViewModel
            {
                Id = mantenPreventivo.Id,
                AspNetEquipoId = mantenPreventivo.EquipoEntity.Id.ToString(),
                AspNetUsuariosId = mantenPreventivo.UsuarioEntity.Id.ToString(),
                AspNetEquipos = _combosHelper.GetComboEquipos(),
                AspNetUsuarios = _combosHelper.GetComboUsuarios(),
                Observaciones = mantenPreventivo.Observaciones,
                Status = mantenPreventivo.Status,
                AspNetFrecuenciaId = mantenPreventivo.Frecuencia,
                AspNetFrecuencias = _combosHelper.GetComboFrecuencias(),
                Dia = mantenPreventivo.Frecuencia,
                MantenPreventivoId = mantenPreventivo.MantenPreventivoId,
            };



            return View(model);
        }

        // POST: MantenPreventivos/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, MantenPreventivosViewModel model)
        {
            if (id != model.Id)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {

                    var equipo = _dataContext.Equipos.FirstOrDefault(o => o.Id.ToString() == model.AspNetEquipoId);
                    var user = _dataContext.Usuarios.FirstOrDefault(o => o.Id.ToString() == model.AspNetUsuariosId);

                    var mantenPreventivo = new MantenPreventivo
                    {
                        Id = model.Id,
                        EquipoEntity = equipo,
                        MantenPreventivoId = model.MantenPreventivoId,
                        Observaciones = model.Observaciones,
                        Dia = model.Dia,
                        Frecuencia = model.AspNetFrecuenciaId,
                        Status = "Activo",
                        UsuarioEntity = user,
                    };

                    _dataContext.Update(mantenPreventivo);
                    await _dataContext.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!MantenPreventivoExists(model.Id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            return View(model);
        }

        // GET: MantenPreventivos/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var mantenPreventivo = await _dataContext.MantenPreventivo
                .FirstOrDefaultAsync(m => m.Id == id);
            if (mantenPreventivo == null)
            {
                return NotFound();
            }

            return View(mantenPreventivo);
        }

        // POST: MantenPreventivos/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var mantenPreventivo = await _dataContext.MantenPreventivo.FindAsync(id);
            _dataContext.MantenPreventivo.Remove(mantenPreventivo);
            await _dataContext.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool MantenPreventivoExists(int id)
        {
            return _dataContext.MantenPreventivo.Any(e => e.Id == id);
        }
    }
}
