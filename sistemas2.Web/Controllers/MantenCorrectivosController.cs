﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using sistemas2.Web.Data;
using sistemas2.Web.Data.Entities;
using sistemas2.Web.Helpers;
using sistemas2.Web.Models;

namespace sistemas2.Web.Controllers
{
    public class MantenCorrectivosController : Controller
    {
        private readonly DataContext _dataContext;
        private readonly ICombosHelper _combosHelper;

        public MantenCorrectivosController(DataContext context,
            ICombosHelper combosHelper)
        {
            _dataContext = context;
            _combosHelper = combosHelper;
        }

        // GET: MantenCorrectivos
        public async Task<IActionResult> Index()
        {
            return View(await _dataContext.MantenCorrectivo.Include(o => o.EquipoEntity).Include(o => o.UsuarioEntity).ToListAsync());
        }

        // GET: MantenCorrectivos/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var mantenCorrectivo = await _dataContext.MantenCorrectivo
                .FirstOrDefaultAsync(m => m.Id == id);
            if (mantenCorrectivo == null)
            {
                return NotFound();
            }

            return View(mantenCorrectivo);
        }

        // GET: MantenCorrectivos/Create
        public IActionResult Create()
        {
            var max = _dataContext.MantenCorrectivo.Count();

            var model = new MantenCorrectivosViewModel
            {
                FechaMantenCorrectivo = DateTime.Now,
                MantenCorrectivoId = Convert.ToInt32(max),
                AspNetUsuarios = _combosHelper.GetComboUsuarios(),
                AspNetEquipos = _combosHelper.GetComboEquipos(),
                Status = "Activo",
            };
            return View(model);
        }

        // POST: MantenCorrectivos/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create(MantenCorrectivosViewModel model)
        {
            if (ModelState.IsValid)
            {

                var equipo = _dataContext.Equipos.FirstOrDefault(o => o.Id.ToString() == model.AspNetEquipoId);
                var user = _dataContext.Usuarios.FirstOrDefault(o => o.Id.ToString() == model.AspNetUsuariosId);

                var mantenCorrectivo = new MantenCorrectivo
                {
                    Id = model.Id,
                    EquipoEntity = equipo,
                    FechaMantenCorrectivo = model.FechaMantenCorrectivo,
                    MantenCorrectivoId = model.MantenCorrectivoId,
                    Motivo = model.Motivo,
                    Solucion = model.Solucion,
                    Status = "Activo",
                    UsuarioEntity = user,
                };

                _dataContext.Add(mantenCorrectivo);

                try
                {
                    await _dataContext.SaveChangesAsync();

                    return RedirectToAction(nameof(Index));
                }
                catch (Exception ex)
                {
                    ModelState.AddModelError(string.Empty, ex.ToString());
                    return View(model);
                }
            }
            return View(model);
        }

        // GET: MantenCorrectivos/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var mantenCorrectivo = await _dataContext.MantenCorrectivo.Include(o => o.EquipoEntity).Include(o => o.UsuarioEntity)
                .FirstOrDefaultAsync(m => m.Id == id);

            if (mantenCorrectivo == null)
            {
                return NotFound();
            }

            var model = new MantenCorrectivosViewModel
            {
                Id = mantenCorrectivo.Id,
                AspNetEquipoId = mantenCorrectivo.EquipoEntity.Id.ToString(),
                AspNetUsuariosId = mantenCorrectivo.UsuarioEntity.Id.ToString(),
                FechaMantenCorrectivo = mantenCorrectivo.FechaMantenCorrectivo,
                AspNetEquipos = _combosHelper.GetComboEquipos(),
                AspNetUsuarios = _combosHelper.GetComboUsuarios(),
                Motivo = mantenCorrectivo.Motivo,
                Solucion = mantenCorrectivo.Solucion,
                Status = mantenCorrectivo.Status,
                MantenCorrectivoId = mantenCorrectivo.MantenCorrectivoId,
            };



            return View(model);
        }

        // POST: MantenCorrectivos/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, MantenCorrectivosViewModel model)
        {
            if (id != model.Id)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {

                    var equipo = _dataContext.Equipos.FirstOrDefault(o => o.Id.ToString() == model.AspNetEquipoId);
                    var user = _dataContext.Usuarios.FirstOrDefault(o => o.Id.ToString() == model.AspNetUsuariosId);

                    var mantenCorrectivo = new MantenCorrectivo
                    {
                        Id = model.Id,
                        EquipoEntity = equipo,
                        FechaMantenCorrectivo = model.FechaMantenCorrectivo,
                        MantenCorrectivoId = model.MantenCorrectivoId,
                        Motivo = model.Motivo,
                        Solucion = model.Solucion,
                        Status = "Activo",
                        UsuarioEntity = user,
                    };

                    _dataContext.Update(mantenCorrectivo);
                    await _dataContext.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!MantenCorrectivoExists(model.Id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            return View(model);
        }

        // GET: MantenCorrectivos/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var mantenCorrectivo = await _dataContext.MantenCorrectivo
                .FirstOrDefaultAsync(m => m.Id == id);
            if (mantenCorrectivo == null)
            {
                return NotFound();
            }

            return View(mantenCorrectivo);
        }

        // POST: MantenCorrectivos/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var mantenCorrectivo = await _dataContext.MantenCorrectivo.FindAsync(id);
            _dataContext.MantenCorrectivo.Remove(mantenCorrectivo);
            await _dataContext.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool MantenCorrectivoExists(int id)
        {
            return _dataContext.MantenCorrectivo.Any(e => e.Id == id);
        }
    }
}
