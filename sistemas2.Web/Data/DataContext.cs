﻿using Microsoft.EntityFrameworkCore;
using sistemas2.Web.Data.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace sistemas2.Web.Data
{
    public class DataContext:DbContext
    {
        public DataContext(DbContextOptions<DataContext> options): base(options)
        {
        }

        public DbSet<SedeEntity> Sedes { get; set; }
        public DbSet<TipoEntity> Tipos { get; set; }
        public DbSet<HardwareEntity> Hardwares { get; set; }
        public DbSet<SoftwareEntity> Softwares { get; set; }
        public DbSet<EquipoEntity> Equipos { get; set; }     
        public DbSet<EquipoHarwareEntity> EquipoHardwares { get; set; }
        public DbSet<EquipoSoftwareEntity> EquipoSoftwares { get; set; }

        public DbSet<UsuarioEntity> Usuarios { get; set; }

        //public DbSet<MantenimientoEntity> Mantenimientos { get; set; }

        public DbSet<HistorialEquiUsEntity> HistorialEquiUsEntity { get; set; }

        public DbSet<TrasladoEntity> Traslados { get; set; }

        public DbSet<TrasladoDetalleEntity> TrasladosDetalles { get; set; }

        public DbSet<sistemas2.Web.Data.Entities.MantenPreventivo> MantenPreventivo { get; set; }

        public DbSet<sistemas2.Web.Data.Entities.MantenCorrectivo> MantenCorrectivo { get; set; }


    }
}
