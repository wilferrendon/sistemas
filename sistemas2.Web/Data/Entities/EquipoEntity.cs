﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace sistemas2.Web.Data.Entities
{
    public class EquipoEntity
    {
        public int Id { get; set; }

        [Required(ErrorMessage = "El campo {0} es obligatorio.")]
        public int Codigo { get; set; }

        [StringLength(40, MinimumLength = 1, ErrorMessage ="El campo {0} debe tener al menos {1} caracteres.")]
        [Required(ErrorMessage ="El campo {0} es obligatorio.")]
        public string Marca { get; set; }

        [StringLength(40, MinimumLength = 1, ErrorMessage = "El campo {0} debe tener al menos {1} caracteres.")]
        [Required(ErrorMessage = "El campo {0} es obligatorio.")]
        
        public string Description { get; set; }

        public string DescriptionDetallada { get; set; }

        public string Serial { get; set; }

        public DateTime Fecha_Compra { get; set; }

        [Display(Name = "Fecha Compra")]
        [Required(ErrorMessage = "El campo {0} es obligatorio.")]
        [DisplayFormat(DataFormatString = "{0:yyyy/MM/dd}", ApplyFormatInEditMode = true)]
        public DateTime Fecha_CompraLocal => Fecha_Compra.ToLocalTime();

        [Display(Name = "Valor")]
        [Required(ErrorMessage = "El campo {0} es obligatorio.")]
        [DisplayFormat(DataFormatString = "{0:C2}", ApplyFormatInEditMode = false)]
        public decimal Valor { get; set; }

        [Display(Name = "Imagen Factura")]
        public string InvoicePath { get; set; }

        [Display(Name = "Imagen Equipo")]
        public string EquipoPath { get; set; }

        [Required(ErrorMessage = "El campo {0} es obligatorio.")]
        public TipoEntity TipoEntity { get; set; }
        [Required(ErrorMessage = "El campo {0} es obligatorio.")]
        
        public SedeEntity SedeEntity { get; set; }

        public UsuarioEntity UsuarioEntity { get; set; }

        public ICollection<EquipoHarwareEntity> EqHardwares { get; set; }
        public ICollection<EquipoSoftwareEntity> EqSoftwares { get; set; }

        public ICollection<TrasladoDetalleEntity> TrasladoDetalles { get; set; }
        public ICollection<MantenCorrectivo> MantenCorrectivos { get; set; }

        public ICollection<MantenPreventivo> MantenPreventivos { get; set; }

    }
}
