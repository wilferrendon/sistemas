﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace sistemas2.Web.Data.Entities
{
    public class Frecuencias
    {
        public int Id { get; set; }
        public string Frecuencia { get; set; }
    }
}
