﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace sistemas2.Web.Data.Entities
{
    public class TrasladoEntity
    {
        public int Id { get; set; }

        [StringLength(50, MinimumLength = 1, ErrorMessage = "El campo {0} debe tener al menos {1} caracteres.")]
        [Required(ErrorMessage = "El campo {0} es obligatorio.")]
        public string Traslado { get; set; }

        [Required(ErrorMessage = "El campo {0} es obligatorio.")]
        public DateTime FechaTraslado { get; set; }

        public DateTime FechaRecibido { get; set; }

        public DateTime Last_Updated { get; set; }

        public string Last_User_By { get; set; }
        public string Status { get; set; }

        [Required(ErrorMessage = "El campo {0} es obligatorio.")]
        public string SedeOrigen { get; set; }
        [Required(ErrorMessage = "El campo {0} es obligatorio.")]
        public string SedeDestino { get; set; }
        [Required(ErrorMessage = "El campo {0} es obligatorio.")]
        public int UsuarioOrigen { get; set; }

        [Required(ErrorMessage = "El campo {0} es obligatorio.")]
        public int UsuarioDestino { get; set; }

        [Display(Name = "Motivo")]
        [Required(ErrorMessage = "El campo {0} es obligatorio.")]
        public string Motivo { get; set; }

        public ICollection<TrasladoDetalleEntity> TrasladosDetalles { get; set; }

    }

}
