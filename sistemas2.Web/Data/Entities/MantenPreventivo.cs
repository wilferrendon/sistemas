﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace sistemas2.Web.Data.Entities
{
    public class MantenPreventivo
    {
        public int Id { get; set; }

        [Required(ErrorMessage = "El campo {0} es obligatorio.")]

        public int MantenPreventivoId { get; set; }

        [Required(ErrorMessage = "El campo {0} es obligatorio.")]

        public EquipoEntity EquipoEntity { get; set; }

        [Required(ErrorMessage = "El campo {0} es obligatorio.")]

        public UsuarioEntity UsuarioEntity { get; set; }

        [Required(ErrorMessage = "El campo {0} es obligatorio.")]
        public string Status { get; set; }

        [Required(ErrorMessage = "El campo {0} es obligatorio.")]
        public string Frecuencia { get; set; }

        public string Dia { get; set; }

        [Required(ErrorMessage = "El campo {0} es obligatorio.")]
        public string Observaciones { get; set; }

    }
}
