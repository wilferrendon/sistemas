﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace sistemas2.Web.Data.Entities
{
    public class SoftwareEntity
    {
        public int Id { get; set; }

        [StringLength(40, MinimumLength = 1, ErrorMessage = "El campo {0} debe tener al menos {1} caracteres.")]
        [Required(ErrorMessage = "El campo {0} es obligatorio.")]
        public string Description { get; set; }

        public string DescriptionDetallada { get; set; }

        public string Serial { get; set; }

        public TipoEntity TipoEntity { get; set; }

        public ICollection<EquipoSoftwareEntity> Softwares { get; set; }
    }
}
