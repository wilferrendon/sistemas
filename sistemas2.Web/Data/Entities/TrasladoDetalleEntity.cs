﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace sistemas2.Web.Data.Entities
{
    public class TrasladoDetalleEntity
    {
        public int Id { get; set; }

        [StringLength(50, MinimumLength = 1, ErrorMessage = "El campo {0} debe tener al menos {1} caracteres.")]
        [Required(ErrorMessage = "El campo {0} es obligatorio.")]
       
        public string Consecutivo { get; set; }

        [Required(ErrorMessage = "El campo {0} es obligatorio.")]
        public TrasladoEntity TrasladoEntity { get; set; }

        [Required(ErrorMessage = "El campo {0} es obligatorio.")]
        public EquipoEntity EquipoEntity { get; set; }
    }
}
