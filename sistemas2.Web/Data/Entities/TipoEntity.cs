﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace sistemas2.Web.Data.Entities
{
    public class TipoEntity
    {
        public int Id { get; set; }

        [StringLength(20, MinimumLength = 1, ErrorMessage = "El campo {0} debe tener al menos {1} caracteres.")]
        [Required(ErrorMessage = "El campo {0} es obligatorio.")]

        [Display(Name = "Tipo")]
        public string Tipo_Id { get; set; }

        [Display(Name = "Descripcion")]
        [StringLength(30, MinimumLength = 1, ErrorMessage = "El campo {0} debe tener al menos {1} caracteres.")]
        [Required(ErrorMessage = "El campo {0} es obligatorio.")]
        public string Description { get; set; }

        public ICollection<SoftwareEntity> Softwares { get; set; }
        public ICollection<HardwareEntity> Hardwares { get; set; }
        public ICollection<EquipoEntity> Equipos { get; set; }

    }
}
