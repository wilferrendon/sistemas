﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace sistemas2.Web.Data.Entities
{
    public class EquipoHarwareEntity
    {
        public int Id { get; set; }
        [Required(ErrorMessage = "El campo {0} es obligatorio.")]
        public EquipoEntity EquipoEntity { get; set; }

        [Required(ErrorMessage = "El campo {0} es obligatorio.")]
        public HardwareEntity HardwareEntity { get; set; }


    }
}
