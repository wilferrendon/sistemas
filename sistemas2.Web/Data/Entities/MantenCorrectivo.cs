﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace sistemas2.Web.Data.Entities
{
    public class MantenCorrectivo
    {
        public int Id { get; set; }

        [Required(ErrorMessage = "El campo {0} es obligatorio.")]

        public int MantenCorrectivoId { get; set; }

        public DateTime FechaMantenCorrectivo { get; set; }

        [Required(ErrorMessage = "El campo {0} es obligatorio.")]

        public EquipoEntity EquipoEntity { get; set; }

        [Required(ErrorMessage = "El campo {0} es obligatorio.")]
        
        public UsuarioEntity UsuarioEntity { get; set; }

        [Required(ErrorMessage = "El campo {0} es obligatorio.")]
        public string Status { get; set; }

        [Required(ErrorMessage = "El campo {0} es obligatorio.")]
        public string Motivo { get; set; }

        [Required(ErrorMessage = "El campo {0} es obligatorio.")]
        public string Solucion { get; set; }

    }
}
