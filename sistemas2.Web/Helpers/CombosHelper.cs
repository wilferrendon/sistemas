﻿using Microsoft.AspNetCore.Mvc.Rendering;
using sistemas2.Web.Data;
using sistemas2.Web.Data.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace sistemas2.Web.Helpers
{
    public class CombosHelper:ICombosHelper
    {
        private readonly DataContext _dataContext;

        public CombosHelper(DataContext dataContext)
        {
            _dataContext = dataContext;
        }
        public IEnumerable<SelectListItem> GetComboTipoEquipos(string opcion)
        {
            var list = _dataContext.Tipos.Where(e => e.Tipo_Id == opcion)
                .Select(pt => new SelectListItem
                {
                    Text = pt.Description,
                    Value = $"{pt.Id}"
                })
                .OrderBy(pt => pt.Text)
                .ToList();

            list.Insert(0, new SelectListItem
            {
                Text = "[Seleccione un tipo...]",
                Value = "0"
            });

            return list;
        }

        public IEnumerable<SelectListItem> GetComboUsuarios()
        {
            var list = _dataContext.Usuarios
                .Select(pt => new SelectListItem
                {
                    Text = pt.Nombres + " " + pt.Apellidos,
                    Value = $"{pt.Id}"
                })
                .OrderBy(pt => pt.Text)
                .ToList();

            list.Insert(0, new SelectListItem
            {
                Text = "[Seleccione un usuario...]",
                Value = "0"
            });

            return list;
        }

        public IEnumerable<SelectListItem> GetComboSedes()
        {
            var list = _dataContext.Sedes
                .Select(pt => new SelectListItem
                {
                    Text = pt.Description,
                    Value = $"{pt.Id}"
                })
                .OrderBy(pt => pt.Text)
                .ToList();

            list.Insert(0, new SelectListItem
            {
                Text = "[Seleccione una sede...]",
                Value = "0"
            });

            return list;
        }

        public IEnumerable<SelectListItem> GetComboHardwares()
        {
            var list = _dataContext.Hardwares
                .Select(pt => new SelectListItem
                {
                    Text = pt.Description,
                    Value = $"{pt.Id}"
                })
                .OrderBy(pt => pt.Text)
                .ToList();

            list.Insert(0, new SelectListItem
            {
                Text = "[Seleccione un hardware...]",
                Value = "0"
            });

            return list;
        }

        public IEnumerable<SelectListItem> GetComboSoftwares()
        {
            var list = _dataContext.Softwares
                .Select(pt => new SelectListItem
                {
                    Text = pt.Description,
                    Value = $"{pt.Id}"
                })
                .OrderBy(pt => pt.Text)
                .ToList();

            list.Insert(0, new SelectListItem
            {
                Text = "[Seleccione un software...]",
                Value = "0"
            });

            return list;
        }
        public IEnumerable<SelectListItem> GetComboEquipos()
        {
            var list = _dataContext.Equipos
                .Select(pt => new SelectListItem
                {
                    Text = pt.Description,
                    Value = $"{pt.Id}"
                })
                .OrderBy(pt => pt.Text)
                .ToList();

            list.Insert(0, new SelectListItem
            {
                Text = "[Seleccione un equipo...]",
                Value = "0"
            });

            return list;
        }

        public IEnumerable<SelectListItem> GetComboFrecuencias()
        {


            var lista = new List<Frecuencias>();

            var frec = new Frecuencias();
            frec.Id = 1;
            frec.Frecuencia ="Diaria";

            lista.Add(frec);

            frec = new Frecuencias();
            frec.Id = 2;
            frec.Frecuencia = "Semanal";

            lista.Add(frec);

            frec = new Frecuencias();
            frec.Id = 3;
            frec.Frecuencia = "Quincenal";

            lista.Add(frec);

            frec = new Frecuencias();
            frec.Id = 4;
            frec.Frecuencia = "Mensual";

            lista.Add(frec);

            frec = new Frecuencias();
            frec.Id = 5;
            frec.Frecuencia = "Trimestral";

            lista.Add(frec);

            frec = new Frecuencias();
            frec.Id = 6;
            frec.Frecuencia = "Semestral";

            lista.Add(frec);

            frec = new Frecuencias();
            frec.Id = 7;
            frec.Frecuencia = "Anual";

            lista.Add(frec);

                       
            var list = lista
                .Select(pt => new SelectListItem
                {
                    Text = pt.Frecuencia,
                    Value = $"{pt.Id}"
                })
                .OrderBy(pt => pt.Text)
                .ToList();

            list.Insert(0, new SelectListItem
            {
                Text = "[Seleccione una frecuencia...]",
                Value = "0"
            });

            return list;
        }

    }
}
