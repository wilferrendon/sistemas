﻿using Microsoft.AspNetCore.Mvc.Rendering;
using sistemas2.Web.Data.Entities;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace sistemas2.Web.Models
{
    public class TrasladosViewModel
    {
        public int Id { get; set; }

        [StringLength(50, MinimumLength = 1, ErrorMessage = "El campo {0} debe tener al menos {1} caracteres.")]
        [Required(ErrorMessage = "El campo {0} es obligatorio.")]
        public string Traslado { get; set; }

        [Required(ErrorMessage = "El campo {0} es obligatorio.")]
        public DateTime FechaTraslado { get; set; }

        [Required(ErrorMessage = "El campo {0} es obligatorio.")]
        [Display(Name = "Sede Origen")]
        public string AspNetSedesOrigenId { get; set; }
        public IEnumerable<SelectListItem> AspNetSedes { get; set; }

        [Required(ErrorMessage = "El campo {0} es obligatorio.")]
        [Display(Name = "Sede Destino")]
        public string AspNetSedesDestinoId { get; set; }
        public IEnumerable<SelectListItem> AspNetSedes2 { get; set; }

        [Required(ErrorMessage = "El campo {0} es obligatorio.")]
        [Display(Name = "Usuario Origen")]
        public string AspNetUsuariosOrigenId { get; set; }
        public IEnumerable<SelectListItem> AspNetUsuarios { get; set; }

        [Required(ErrorMessage = "El campo {0} es obligatorio.")]
        [Display(Name = "Usuario Destino")]
        public string AspNetUsuariosDestinoId { get; set; }
        public IEnumerable<SelectListItem> AspNetUsuarios2 { get; set; }

        [Display(Name = "Motivo")]
        [Required(ErrorMessage = "El campo {0} es obligatorio.")]
        public string Motivo { get; set; }

        public ICollection<TrasladoDetalleEntity> TrasladosDetalles { get; set; }

       
    }
}
