﻿using Microsoft.AspNetCore.Mvc.Rendering;
using sistemas2.Web.Data.Entities;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace sistemas2.Web.Models
{
    public class EquipoHardwaresViewModel
    {
        public int Id { get; set; }
       
        [Required(ErrorMessage = "El campo {0} es obligatorio.")]

        public int EquipoId;
        [Required(ErrorMessage = "El campo {0} es obligatorio.")]
        public string Description;

        [Display(Name = "Hardware")]
        public string AspNetHardwareId { get; set; }
        public IEnumerable<SelectListItem> AspNetHardwares { get; set; }

    }
}
