﻿using Microsoft.AspNetCore.Mvc.Rendering;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace sistemas2.Web.Models
{
    public class MantenPreventivosViewModel
    {
        public int Id { get; set; }

        [Required(ErrorMessage = "El campo {0} es obligatorio.")]

        public int MantenPreventivoId { get; set; }

        [Display(Name = "Equipos")]
        public string AspNetEquipoId { get; set; }
        public IEnumerable<SelectListItem> AspNetEquipos { get; set; }

        [Required(ErrorMessage = "El campo {0} es obligatorio.")]
        [Display(Name = "Usuario Origen")]
        public string AspNetUsuariosId { get; set; }
        public IEnumerable<SelectListItem> AspNetUsuarios { get; set; }

        [Required(ErrorMessage = "El campo {0} es obligatorio.")]
        public string Status { get; set; }

        [Required(ErrorMessage = "El campo {0} es obligatorio.")]
        [Display(Name = "Usuario Origen")]
        public string AspNetFrecuenciaId { get; set; }
        public IEnumerable<SelectListItem> AspNetFrecuencias { get; set; }

        public string Dia { get; set; }

        [Required(ErrorMessage = "El campo {0} es obligatorio.")]
        public string Observaciones { get; set; }
    }
}
