﻿using Microsoft.AspNetCore.Mvc.Rendering;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace sistemas2.Web.Models
{
    public class TrasladoDetallesViewModel
    {
        public int Id { get; set; }
        
        [Required(ErrorMessage = "El campo {0} es obligatorio.")]

        public int TrasladoId;

        [StringLength(50, MinimumLength = 1, ErrorMessage = "El campo {0} debe tener al menos {1} caracteres.")]
        [Required(ErrorMessage = "El campo {0} es obligatorio.")]

        public string Consecutivo { get; set; }

        [Display(Name = "Equipos")]
        public string AspNetEquipoId { get; set; }
        public IEnumerable<SelectListItem> AspNetEquipos { get; set; }
    }
}
