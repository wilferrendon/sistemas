﻿using Microsoft.AspNetCore.Mvc.Rendering;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace sistemas2.Web.Models
{
    public class SoftwaresViewModel
    {
        public int Id { get; set; }

        [Display(Name = "Descripcion")]
        [StringLength(40, MinimumLength = 1, ErrorMessage = "El campo {0} debe tener al menos {1} caracteres.")]
        [Required(ErrorMessage = "El campo {0} es obligatorio.")]
        public string Description { get; set; }

        [Display(Name = "Descripcion Detallada")]
        public string DescriptionDetallada { get; set; }

        public string Serial { get; set; }

        [Display(Name = "Tipo")]
        [Required(ErrorMessage = "El campo {0} es obligatorio.")]
        public string AspNetTypeId { get; set; }
        public IEnumerable<SelectListItem> AspNetTypes { get; set; }
    }

}
