﻿using System;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;

namespace sistemas2.Web.Migrations
{
    public partial class initialdb : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "HistorialEquiUsEntity",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Equipo = table.Column<string>(nullable: true),
                    Usuario = table.Column<string>(nullable: true),
                    Fecha_Inicio = table.Column<DateTime>(nullable: false),
                    Fecha_Final = table.Column<DateTime>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_HistorialEquiUsEntity", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Sedes",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Description = table.Column<string>(maxLength: 40, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Sedes", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Tipos",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Tipo_Id = table.Column<string>(maxLength: 20, nullable: false),
                    Description = table.Column<string>(maxLength: 30, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Tipos", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Traslados",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Traslado = table.Column<string>(maxLength: 50, nullable: false),
                    FechaTraslado = table.Column<DateTime>(nullable: false),
                    FechaRecibido = table.Column<DateTime>(nullable: false),
                    Last_Updated = table.Column<DateTime>(nullable: false),
                    Last_User_By = table.Column<string>(nullable: true),
                    Status = table.Column<string>(nullable: true),
                    SedeOrigen = table.Column<string>(nullable: false),
                    SedeDestino = table.Column<string>(nullable: false),
                    UsuarioOrigen = table.Column<int>(nullable: false),
                    UsuarioDestino = table.Column<int>(nullable: false),
                    Motivo = table.Column<string>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Traslados", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Usuarios",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Cedula = table.Column<int>(nullable: false),
                    Nombres = table.Column<string>(maxLength: 40, nullable: false),
                    Apellidos = table.Column<string>(maxLength: 40, nullable: false),
                    Cargo = table.Column<string>(maxLength: 50, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Usuarios", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Hardwares",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Description = table.Column<string>(maxLength: 50, nullable: false),
                    Serial = table.Column<string>(nullable: true),
                    TipoEntityId = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Hardwares", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Hardwares_Tipos_TipoEntityId",
                        column: x => x.TipoEntityId,
                        principalTable: "Tipos",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "Softwares",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Description = table.Column<string>(maxLength: 40, nullable: false),
                    DescriptionDetallada = table.Column<string>(nullable: true),
                    Serial = table.Column<string>(nullable: true),
                    TipoEntityId = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Softwares", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Softwares_Tipos_TipoEntityId",
                        column: x => x.TipoEntityId,
                        principalTable: "Tipos",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "Equipos",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Codigo = table.Column<int>(nullable: false),
                    Marca = table.Column<string>(maxLength: 40, nullable: false),
                    Description = table.Column<string>(maxLength: 40, nullable: false),
                    DescriptionDetallada = table.Column<string>(nullable: true),
                    Serial = table.Column<string>(nullable: true),
                    Fecha_Compra = table.Column<DateTime>(nullable: false),
                    Valor = table.Column<decimal>(nullable: false),
                    InvoicePath = table.Column<string>(nullable: true),
                    EquipoPath = table.Column<string>(nullable: true),
                    TipoEntityId = table.Column<int>(nullable: false),
                    SedeEntityId = table.Column<int>(nullable: false),
                    UsuarioEntityId = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Equipos", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Equipos_Sedes_SedeEntityId",
                        column: x => x.SedeEntityId,
                        principalTable: "Sedes",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_Equipos_Tipos_TipoEntityId",
                        column: x => x.TipoEntityId,
                        principalTable: "Tipos",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_Equipos_Usuarios_UsuarioEntityId",
                        column: x => x.UsuarioEntityId,
                        principalTable: "Usuarios",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "EquipoHardwares",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    EquipoEntityId = table.Column<int>(nullable: false),
                    HardwareEntityId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_EquipoHardwares", x => x.Id);
                    table.ForeignKey(
                        name: "FK_EquipoHardwares_Equipos_EquipoEntityId",
                        column: x => x.EquipoEntityId,
                        principalTable: "Equipos",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_EquipoHardwares_Hardwares_HardwareEntityId",
                        column: x => x.HardwareEntityId,
                        principalTable: "Hardwares",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "EquipoSoftwares",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    EquipoEntityId = table.Column<int>(nullable: false),
                    SoftwareEntityId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_EquipoSoftwares", x => x.Id);
                    table.ForeignKey(
                        name: "FK_EquipoSoftwares_Equipos_EquipoEntityId",
                        column: x => x.EquipoEntityId,
                        principalTable: "Equipos",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_EquipoSoftwares_Softwares_SoftwareEntityId",
                        column: x => x.SoftwareEntityId,
                        principalTable: "Softwares",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "MantenCorrectivo",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    MantenCorrectivoId = table.Column<int>(nullable: false),
                    FechaMantenCorrectivo = table.Column<DateTime>(nullable: false),
                    EquipoEntityId = table.Column<int>(nullable: false),
                    UsuarioEntityId = table.Column<int>(nullable: false),
                    Status = table.Column<string>(nullable: false),
                    Motivo = table.Column<string>(nullable: false),
                    Solucion = table.Column<string>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_MantenCorrectivo", x => x.Id);
                    table.ForeignKey(
                        name: "FK_MantenCorrectivo_Equipos_EquipoEntityId",
                        column: x => x.EquipoEntityId,
                        principalTable: "Equipos",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_MantenCorrectivo_Usuarios_UsuarioEntityId",
                        column: x => x.UsuarioEntityId,
                        principalTable: "Usuarios",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "MantenPreventivo",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    MantenPreventivoId = table.Column<int>(nullable: false),
                    EquipoEntityId = table.Column<int>(nullable: false),
                    UsuarioEntityId = table.Column<int>(nullable: false),
                    Status = table.Column<string>(nullable: false),
                    Frecuencia = table.Column<string>(nullable: false),
                    Dia = table.Column<string>(nullable: true),
                    Observaciones = table.Column<string>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_MantenPreventivo", x => x.Id);
                    table.ForeignKey(
                        name: "FK_MantenPreventivo_Equipos_EquipoEntityId",
                        column: x => x.EquipoEntityId,
                        principalTable: "Equipos",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_MantenPreventivo_Usuarios_UsuarioEntityId",
                        column: x => x.UsuarioEntityId,
                        principalTable: "Usuarios",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "TrasladosDetalles",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Consecutivo = table.Column<string>(maxLength: 50, nullable: false),
                    TrasladoEntityId = table.Column<int>(nullable: false),
                    EquipoEntityId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_TrasladosDetalles", x => x.Id);
                    table.ForeignKey(
                        name: "FK_TrasladosDetalles_Equipos_EquipoEntityId",
                        column: x => x.EquipoEntityId,
                        principalTable: "Equipos",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_TrasladosDetalles_Traslados_TrasladoEntityId",
                        column: x => x.TrasladoEntityId,
                        principalTable: "Traslados",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_EquipoHardwares_EquipoEntityId",
                table: "EquipoHardwares",
                column: "EquipoEntityId");

            migrationBuilder.CreateIndex(
                name: "IX_EquipoHardwares_HardwareEntityId",
                table: "EquipoHardwares",
                column: "HardwareEntityId");

            migrationBuilder.CreateIndex(
                name: "IX_Equipos_SedeEntityId",
                table: "Equipos",
                column: "SedeEntityId");

            migrationBuilder.CreateIndex(
                name: "IX_Equipos_TipoEntityId",
                table: "Equipos",
                column: "TipoEntityId");

            migrationBuilder.CreateIndex(
                name: "IX_Equipos_UsuarioEntityId",
                table: "Equipos",
                column: "UsuarioEntityId");

            migrationBuilder.CreateIndex(
                name: "IX_EquipoSoftwares_EquipoEntityId",
                table: "EquipoSoftwares",
                column: "EquipoEntityId");

            migrationBuilder.CreateIndex(
                name: "IX_EquipoSoftwares_SoftwareEntityId",
                table: "EquipoSoftwares",
                column: "SoftwareEntityId");

            migrationBuilder.CreateIndex(
                name: "IX_Hardwares_TipoEntityId",
                table: "Hardwares",
                column: "TipoEntityId");

            migrationBuilder.CreateIndex(
                name: "IX_MantenCorrectivo_EquipoEntityId",
                table: "MantenCorrectivo",
                column: "EquipoEntityId");

            migrationBuilder.CreateIndex(
                name: "IX_MantenCorrectivo_UsuarioEntityId",
                table: "MantenCorrectivo",
                column: "UsuarioEntityId");

            migrationBuilder.CreateIndex(
                name: "IX_MantenPreventivo_EquipoEntityId",
                table: "MantenPreventivo",
                column: "EquipoEntityId");

            migrationBuilder.CreateIndex(
                name: "IX_MantenPreventivo_UsuarioEntityId",
                table: "MantenPreventivo",
                column: "UsuarioEntityId");

            migrationBuilder.CreateIndex(
                name: "IX_Softwares_TipoEntityId",
                table: "Softwares",
                column: "TipoEntityId");

            migrationBuilder.CreateIndex(
                name: "IX_TrasladosDetalles_EquipoEntityId",
                table: "TrasladosDetalles",
                column: "EquipoEntityId");

            migrationBuilder.CreateIndex(
                name: "IX_TrasladosDetalles_TrasladoEntityId",
                table: "TrasladosDetalles",
                column: "TrasladoEntityId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "EquipoHardwares");

            migrationBuilder.DropTable(
                name: "EquipoSoftwares");

            migrationBuilder.DropTable(
                name: "HistorialEquiUsEntity");

            migrationBuilder.DropTable(
                name: "MantenCorrectivo");

            migrationBuilder.DropTable(
                name: "MantenPreventivo");

            migrationBuilder.DropTable(
                name: "TrasladosDetalles");

            migrationBuilder.DropTable(
                name: "Hardwares");

            migrationBuilder.DropTable(
                name: "Softwares");

            migrationBuilder.DropTable(
                name: "Equipos");

            migrationBuilder.DropTable(
                name: "Traslados");

            migrationBuilder.DropTable(
                name: "Sedes");

            migrationBuilder.DropTable(
                name: "Tipos");

            migrationBuilder.DropTable(
                name: "Usuarios");
        }
    }
}
